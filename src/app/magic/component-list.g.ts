import { CompanyMaster as CompanyMaster_CompanyMaster } from './WCProgs/CompanyMaster/CompanyMaster.component';
import { Detail as CompanyMaster_Detail_Detail } from './WCProgs/CompanyMaster/Detail/Detail.component';
import { CompanyCM as CompanyCM_CompanyCM } from './WCProgs/CompanyCM/CompanyCM.component';
import { MainPage as MainPage_MainPage } from './WCProgs/MainPage/MainPage.component';
import { Login as Login_Login } from './WCProgs/Login/Login.component';
import { Home as HomePage_Home } from './WCProgs/HomePage/Home.component';
import { PurchaseList as PurchaseList_PurchaseList } from './WCTran/PurchaseList/PurchaseList.component';
import { Detail as PurchaseList_Detail_Detail } from './WCTran/PurchaseList/Detail/Detail.component';
import { CMPurchaseTest as CMPurchase_CMPurchaseTest } from './WCTran/CMPurchase/CMPurchaseTest.component';
import { Details as CMPurchase_Details_Details } from './WCTran/CMPurchase/Details/Details.component';
import { BranchSelection as BranchSelection_BranchSelection } from './WCSelection/BranchSelection/BranchSelection.component';
import { Detail as BranchSelection_Detail_Detail } from './WCSelection/BranchSelection/Detail/Detail.component';
import { CustomerSupplierSelection as CustomerSupplierSelection_CustomerSupplierSelection } from './WCSelection/CustomerSupplierSelection/CustomerSupplierSelection.component';
import { Detail as CustomerSupplierSelection_Detail_Detail } from './WCSelection/CustomerSupplierSelection/Detail/Detail.component';
import { Detail as ItemSelectionPurSearch_Detail_Detail } from './WCSelection/ItemSelectionPurSearch/Detail/Detail.component';
import { Detail as CustomerSupplierList_Detail_Detail } from './WCProgs/CustomerSupplierList/Detail/Detail.component';
import { CustomerSupplierList as CustomerSupplierList_CustomerSupplierList } from './WCProgs/CustomerSupplierList/CustomerSupplierList.component';
import { CreateUser as CreateUserLoginPage_CreateUser } from './WCProgs/CreateUserLoginPage/CreateUser.component';
import { CMCustomerSupplier as CMCustomerSupplier_CMCustomerSupplier } from './WCProgs/CMCustomerSupplier/CMCustomerSupplier.component';
import { ViewItemMaster as ItemMasterList_ViewItemMaster } from './WCProgs/ItemMasterList/ViewItemMaster.component';
import { Detail as ItemMasterList_Detail_Detail } from './WCProgs/ItemMasterList/Detail/Detail.component';
import { CMItemMaster as CMItemMaster_CMItemMaster } from './WCProgs/CMItemMaster/CMItemMaster.component';
import { ViewTaxMaster as TaxMasterList_ViewTaxMaster } from './WCProgs/TaxMasterList/ViewTaxMaster.component';
import { Detail as TaxMasterList_Detail_Detail } from './WCProgs/TaxMasterList/Detail/Detail.component';
import { CMTaxMaster as CMTaxMaster_CMTaxMaster } from './WCProgs/CMTaxMaster/CMTaxMaster.component';
import { CTaxType as CMTaxMaster_TaxType_CTaxType } from './WCProgs/CMTaxMaster/TaxType/CTaxType.component';
import { ViewCountryEntry as CountryList_ViewCountryEntry } from './WCProgs/CountryList/ViewCountryEntry.component';
import { Details as CountryList_Details_Details } from './WCProgs/CountryList/Details/Details.component';
import { CMCountry as CMCountry_CMCountry } from './WCProgs/CMCountry/CMCountry.component';
import { ViewStates as StatesList_ViewStates } from './WCProgs/StatesList/ViewStates.component';
import { Details as StatesList_Details_Details } from './WCProgs/StatesList/Details/Details.component';
import { CMStates as CMStates_CMStates } from './WCProgs/CMStates/CMStates.component';
import { CMCity as CMCity_CMCity } from './WCProgs/CMCity/CMCity.component';
import { CityList as CityList_CityList } from './WCProgs/CityList/CityList.component';
import { UnitMaster as UnitMaster_UnitMaster } from './WCProgs/UnitMaster/UnitMaster.component';
import { Detail as UnitMaster_Detail_Detail } from './WCProgs/UnitMaster/Detail/Detail.component';
import { CMUnitMaster as CMUnitMaster_CMUnitMaster } from './WCProgs/CMUnitMaster/CMUnitMaster.component';
import { CompanyBranchList as CompanyBranchList_CompanyBranchList } from './WCProgs/CompanyBranchList/CompanyBranchList.component';
import { Detail as CompanyBranchList_Detail_Detail } from './WCProgs/CompanyBranchList/Detail/Detail.component';
import { CMCompanyBranch as CMCompanyBranch_CMCompanyBranch } from './WCProgs/CMCompanyBranch/CMCompanyBranch.component';
import { FinancialYearList as FinancialYearList_FinancialYearList } from './WCProgs/FinancialYearList/FinancialYearList.component';
import { Details as FinancialYearList_Details_Details } from './WCProgs/FinancialYearList/Details/Details.component';
import { CMFinancialYear as CMFinancialYear_CMFinancialYear } from './WCProgs/CMFinancialYear/CMFinancialYear.component';
import { HSNMasterList as HSNMasterList_HSNMasterList } from './WCProgs/HSNMasterList/HSNMasterList.component';
import { Details as HSNMasterList_Details_Details } from './WCProgs/HSNMasterList/Details/Details.component';
import { CMHSNMaster as CMHSNMaster_CMHSNMaster } from './WCProgs/CMHSNMaster/CMHSNMaster.component';
import { HSNNoSelection as HSNNoSelection_HSNNoSelection } from './WCSelection/HSNNoSelection/HSNNoSelection.component';
import { Detail as HSNNoSelection_Detail_Detail } from './WCSelection/HSNNoSelection/Detail/Detail.component';
import { CountrySelection as CountrySelection_CountrySelection } from './WCSelection/CountrySelection/CountrySelection.component';
import { Details as CountrySelection_Details_Details } from './WCSelection/CountrySelection/Details/Details.component';
import { StatesSelection as StatesSelection_StatesSelection } from './WCSelection/StatesSelection/StatesSelection.component';
import { Details as StatesSelection_Details_Details } from './WCSelection/StatesSelection/Details/Details.component';
import { ItemSelectionPurSearch as ItemSelectionPurSearch_ItemSelectionPurSearch } from './WCSelection/ItemSelectionPurSearch/ItemSelectionPurSearch.component';
import { ItemSelectionSales as ItemSelectionSales_ItemSelectionSales } from './WCSelection/ItemSelectionSales/ItemSelectionSales.component';
import { Detail as ItemSelectionSales_Detail_Detail } from './WCSelection/ItemSelectionSales/Detail/Detail.component';
import { SalesList as SalesList_SalesList } from './WCTran/SalesList/SalesList.component';
import { Detail as SalesList_Detail_Detail } from './WCTran/SalesList/Detail/Detail.component';
import { CMSales as CMSales_CMSales } from './WCTran/CMSales/CMSales.component';
import { Details as CMSales_Details_Details } from './WCTran/CMSales/Details/Details.component';
import { UserCompanyRelation as UserCompanyRelation_UserCompanyRelation } from './WCProgs/UserCompanyRelation/UserCompanyRelation.component';
import { CompanyList as UserCompanyRelation_CompanyList_CompanyList } from './WCProgs/UserCompanyRelation/CompanyList/CompanyList.component';

export const title = "";

export const magicGenCmpsHash = {               UserCompanyRelation_UserCompanyRelation:UserCompanyRelation_UserCompanyRelation,
              UserCompanyRelation_CompanyList_CompanyList:UserCompanyRelation_CompanyList_CompanyList,
                      SalesList_SalesList:SalesList_SalesList,
              SalesList_Detail_Detail:SalesList_Detail_Detail,
              CMSales_CMSales:CMSales_CMSales,
              CMSales_Details_Details:CMSales_Details_Details,
                      ItemSelectionSales_ItemSelectionSales:ItemSelectionSales_ItemSelectionSales,
              ItemSelectionSales_Detail_Detail:ItemSelectionSales_Detail_Detail,
                      ItemSelectionPurSearch_ItemSelectionPurSearch:ItemSelectionPurSearch_ItemSelectionPurSearch,
                      CreateUserLoginPage_CreateUser:CreateUserLoginPage_CreateUser,
              CMCustomerSupplier_CMCustomerSupplier:CMCustomerSupplier_CMCustomerSupplier,
             ItemMasterList_ViewItemMaster:ItemMasterList_ViewItemMaster,
              ItemMasterList_Detail_Detail:ItemMasterList_Detail_Detail,
              CMItemMaster_CMItemMaster:CMItemMaster_CMItemMaster,
              TaxMasterList_ViewTaxMaster:TaxMasterList_ViewTaxMaster,
              TaxMasterList_Detail_Detail:TaxMasterList_Detail_Detail,
              CMTaxMaster_CMTaxMaster:CMTaxMaster_CMTaxMaster,
              CMTaxMaster_TaxType_CTaxType:CMTaxMaster_TaxType_CTaxType,
              CountryList_ViewCountryEntry:CountryList_ViewCountryEntry,
              CountryList_Details_Details:CountryList_Details_Details,
              CMCountry_CMCountry:CMCountry_CMCountry,
              StatesList_ViewStates:StatesList_ViewStates,
              StatesList_Details_Details:StatesList_Details_Details,
              CMStates_CMStates:CMStates_CMStates,
              CMCity_CMCity:CMCity_CMCity,
              CityList_CityList:CityList_CityList,
              UnitMaster_UnitMaster:UnitMaster_UnitMaster,
              UnitMaster_Detail_Detail:UnitMaster_Detail_Detail,
              CMUnitMaster_CMUnitMaster:CMUnitMaster_CMUnitMaster,
              CompanyBranchList_CompanyBranchList:CompanyBranchList_CompanyBranchList,
              CompanyBranchList_Detail_Detail:CompanyBranchList_Detail_Detail,
              CMCompanyBranch_CMCompanyBranch:CMCompanyBranch_CMCompanyBranch,
              FinancialYearList_FinancialYearList:FinancialYearList_FinancialYearList,
              FinancialYearList_Details_Details:FinancialYearList_Details_Details,
              CMFinancialYear_CMFinancialYear:CMFinancialYear_CMFinancialYear,
              HSNMasterList_HSNMasterList:HSNMasterList_HSNMasterList,
              HSNMasterList_Details_Details:HSNMasterList_Details_Details,
              CMHSNMaster_CMHSNMaster:CMHSNMaster_CMHSNMaster,
              HSNNoSelection_HSNNoSelection:HSNNoSelection_HSNNoSelection,
              HSNNoSelection_Detail_Detail:HSNNoSelection_Detail_Detail,
              CountrySelection_CountrySelection:CountrySelection_CountrySelection,
              CountrySelection_Details_Details:CountrySelection_Details_Details,
              StatesSelection_StatesSelection:StatesSelection_StatesSelection,
              StatesSelection_Details_Details:StatesSelection_Details_Details,
                      CustomerSupplierList_CustomerSupplierList:CustomerSupplierList_CustomerSupplierList,
              CustomerSupplierList_Detail_Detail:CustomerSupplierList_Detail_Detail,
                      PurchaseList_PurchaseList:PurchaseList_PurchaseList,
              PurchaseList_Detail_Detail:PurchaseList_Detail_Detail,
              CMPurchase_CMPurchaseTest:CMPurchase_CMPurchaseTest,
              CMPurchase_Details_Details:CMPurchase_Details_Details,
              BranchSelection_BranchSelection:BranchSelection_BranchSelection,
              BranchSelection_Detail_Detail:BranchSelection_Detail_Detail,
              CustomerSupplierSelection_CustomerSupplierSelection:CustomerSupplierSelection_CustomerSupplierSelection,
              CustomerSupplierSelection_Detail_Detail:CustomerSupplierSelection_Detail_Detail,
              ItemSelectionPurSearch_Detail_Detail:ItemSelectionPurSearch_Detail_Detail,
                      HomePage_Home:HomePage_Home,
                      MainPage_MainPage:MainPage_MainPage,
              Login_Login:Login_Login,
                      CompanyMaster_CompanyMaster:CompanyMaster_CompanyMaster,
              CompanyMaster_Detail_Detail:CompanyMaster_Detail_Detail,
              CompanyCM_CompanyCM:CompanyCM_CompanyCM,
       
};

export const magicGenComponents = [ UserCompanyRelation_UserCompanyRelation,
UserCompanyRelation_CompanyList_CompanyList ,  SalesList_SalesList,
SalesList_Detail_Detail,
CMSales_CMSales,
CMSales_Details_Details ,  ItemSelectionSales_ItemSelectionSales,
ItemSelectionSales_Detail_Detail ,  ItemSelectionPurSearch_ItemSelectionPurSearch ,  CreateUserLoginPage_CreateUser,
CMCustomerSupplier_CMCustomerSupplier,
ItemMasterList_ViewItemMaster,
ItemMasterList_Detail_Detail,
CMItemMaster_CMItemMaster,
TaxMasterList_ViewTaxMaster,
TaxMasterList_Detail_Detail,
CMTaxMaster_CMTaxMaster,
CMTaxMaster_TaxType_CTaxType,
CountryList_ViewCountryEntry,
CountryList_Details_Details,
CMCountry_CMCountry,
StatesList_ViewStates,
StatesList_Details_Details,
CMStates_CMStates,
CMCity_CMCity,
CityList_CityList,
UnitMaster_UnitMaster,
UnitMaster_Detail_Detail,
CMUnitMaster_CMUnitMaster,
CompanyBranchList_CompanyBranchList,
CompanyBranchList_Detail_Detail,
CMCompanyBranch_CMCompanyBranch,
FinancialYearList_FinancialYearList,
FinancialYearList_Details_Details,
CMFinancialYear_CMFinancialYear,
HSNMasterList_HSNMasterList,
HSNMasterList_Details_Details,
CMHSNMaster_CMHSNMaster,
HSNNoSelection_HSNNoSelection,
HSNNoSelection_Detail_Detail,
CountrySelection_CountrySelection,
CountrySelection_Details_Details,
StatesSelection_StatesSelection,
StatesSelection_Details_Details ,  CustomerSupplierList_CustomerSupplierList ,
CustomerSupplierList_Detail_Detail ,  PurchaseList_PurchaseList,
PurchaseList_Detail_Detail,
CMPurchase_CMPurchaseTest,
CMPurchase_Details_Details,
BranchSelection_BranchSelection,
BranchSelection_Detail_Detail,
CustomerSupplierSelection_CustomerSupplierSelection,
CustomerSupplierSelection_Detail_Detail,
ItemSelectionPurSearch_Detail_Detail ,  HomePage_Home ,  MainPage_MainPage,
Login_Login ,  CompanyMaster_CompanyMaster,
CompanyMaster_Detail_Detail,
CompanyCM_CompanyCM 
];


export const LazyLoadModulesMap = {};