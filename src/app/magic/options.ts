import { IConfig } from 'ngx-mask';


export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;
