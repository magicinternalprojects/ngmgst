import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        Table4 = "Table4",
        Column5 = "Column5",
        TaxIdEdit = "TaxIdEdit",
        Column6 = "Column6",
        TaxTypeEdit = "TaxTypeEdit",
        Column7 = "Column7",
        DescriptionEdit = "DescriptionEdit",
        Column8 = "Column8",
        TaxPercentageEdit = "TaxPercentageEdit",
        Column9 = "Column9",
        ActiveCheckBox = "ActiveCheckBox",
        Column10 = "Column10",
        CreatedByFullNameEdit = "CreatedByFullNameEdit",
        Column11 = "Column11",
        CreatedDateEdit = "CreatedDateEdit",
        BtnNew = "BtnNew",
        BtnView_Detail = "BtnView_Detail",
        BtnEdit = "BtnEdit",
        BtnHelp = "BtnHelp",
        BtnClose = "BtnClose",        
        Column24 = "Column24",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column5',
        'Column6',
        'Column7',
        'Column8',
        'Column9',
        'Column10',
        'Column11',
        'Column24',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get TaxIdEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.TaxIdEdit);
    }

    get TaxTypeEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.TaxTypeEdit);
    }

    get DescriptionEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.DescriptionEdit);
    }

    get TaxPercentageEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.TaxPercentageEdit);
    }

    get ActiveCheckBox(): FormControl {
        return this.getTableChildFormControl(MgControlName.ActiveCheckBox);
    }

    get CreatedByFullNameEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedByFullNameEdit);
    }

    get CreatedDateEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDateEdit);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}