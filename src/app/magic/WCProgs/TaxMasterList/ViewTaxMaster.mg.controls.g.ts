import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    ViewTaxMaster = "ViewTaxMaster",
        vSearchTextEdit = "vSearchTextEdit",
        BtnSearch = "BtnSearch",
        BtnReset = "BtnReset",
        Detail = "Detail",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get vSearchTextEdit(): FormControl {
        return this.fg.controls[MgControlName.vSearchTextEdit] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}