import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CityList = "CityList",
        Label1048579 = "Label1048579",
        C_Name = "C_Name",
        Label1048581 = "Label1048581",
        S_Name = "S_Name",
        Label1048584 = "Label1048584",
        CityId = "CityId",
        Label1048586 = "Label1048586",
        Name = "Name",
        Label1048588 = "Label1048588",
        ShortName = "ShortName",
        Label1048590 = "Label1048590",
        Description = "Description",
        Label1048591 = "Label1048591",
        Active = "Active",
        Label1048592 = "Label1048592",
        Close = "Close",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get C_Name(): FormControl {
        return this.fg.controls[MgControlName.C_Name] as FormControl;
    }

    get S_Name(): FormControl {
        return this.fg.controls[MgControlName.S_Name] as FormControl;
    }

    get CityId(): FormControl {
        return this.fg.controls[MgControlName.CityId] as FormControl;
    }

    get Name(): FormControl {
        return this.fg.controls[MgControlName.Name] as FormControl;
    }

    get ShortName(): FormControl {
        return this.fg.controls[MgControlName.ShortName] as FormControl;
    }

    get Description(): FormControl {
        return this.fg.controls[MgControlName.Description] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}