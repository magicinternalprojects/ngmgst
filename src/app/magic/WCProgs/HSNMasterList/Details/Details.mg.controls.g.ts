import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Details = "Details",
        Table4 = "Table4",
        Column5 = "Column5",
        RecordIdEdit = "RecordIdEdit",
        Column6 = "Column6",
        HSN_SAC_CodeEdit = "HSN_SAC_CodeEdit",
        Column7 = "Column7",
        HSN_SAC_TypeEdit = "HSN_SAC_TypeEdit",
        Column8 = "Column8",
        DescriptionCGSTEdit = "DescriptionCGSTEdit",
        Column9 = "Column9",
        DescriptionSGSTEdit = "DescriptionSGSTEdit",
        Column10 = "Column10",
        DescriptionIGSTEdit = "DescriptionIGSTEdit",
        Column11 = "Column11",
        DescriptionCessEdit = "DescriptionCessEdit",
        Column12 = "Column12",
        ActiveCheckBox = "ActiveCheckBox",
        Column13 = "Column13",
        CreatedByFullNameEdit = "CreatedByFullNameEdit",
        Column14 = "Column14",
        CreatedDateEdit = "CreatedDateEdit",
        F5Lbl = "F5Lbl",
        gNoteEdit = "gNoteEdit",
        btnAdd = "btnAdd",
        btnEdit = "btnEdit",
        btnView = "btnView",
        btnClose = "btnClose",
        Column15 = "Column15",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column5',
        'Column6',
        'Column7',
        'Column8',
        'Column9',
        'Column10',
        'Column11',
        'Column12',
        'Column13',
        'Column14',
        'Column15',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get RecordIdEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.RecordIdEdit);
    }

    get HSN_SAC_CodeEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.HSN_SAC_CodeEdit);
    }

    get HSN_SAC_TypeEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.HSN_SAC_TypeEdit);
    }

    get DescriptionCGSTEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.DescriptionCGSTEdit);
    }

    get DescriptionSGSTEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.DescriptionSGSTEdit);
    }

    get DescriptionIGSTEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.DescriptionIGSTEdit);
    }

    get DescriptionCessEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.DescriptionCessEdit);
    }

    get ActiveCheckBox(): FormControl {
        return this.getTableChildFormControl(MgControlName.ActiveCheckBox);
    }

    get CreatedByFullNameEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedByFullNameEdit);
    }

    get CreatedDateEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDateEdit);
    }

    get gNoteEdit(): FormControl {
        return this.fg.controls[MgControlName.gNoteEdit] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}