import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Login = "Login",
        vUserName = "vUserName",
        Label1048582 = "Label1048582",
        vPassword = "vPassword",
        Label1048584 = "Label1048584",
        btnLogin = "btnLogin",
        Label1048585 = "Label1048585",
        vCrreateUser = "vCrreateUser",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get vUserName(): FormControl {
        return this.fg.controls[MgControlName.vUserName] as FormControl;
    }

    get vPassword(): FormControl {
        return this.fg.controls[MgControlName.vPassword] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}