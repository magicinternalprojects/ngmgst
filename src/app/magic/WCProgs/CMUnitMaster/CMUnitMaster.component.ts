import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMUnitMaster.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMUnitMaster',
    providers: [...magicProviders],
    templateUrl: './CMUnitMaster.component.html'
})
export class CMUnitMaster extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMUnitMaster";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "50%";
    private static readonly height: string = "300px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;
    X() {
        return CMUnitMaster.x;
    }
    Y() {
        return CMUnitMaster.y;
    }
    Width(): string {
        return CMUnitMaster.width;
    }
    Height(): string {
        return CMUnitMaster.height;
    }
    IsCenteredToWindow() {
        return CMUnitMaster.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CMUnitMaster', 'FormName'));
        //return CMUnitMaster.formName;
    }
    ShowTitleBar() {
        return CMUnitMaster.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMUnitMaster.shouldCloseOnBackgroundClick;
    }
}