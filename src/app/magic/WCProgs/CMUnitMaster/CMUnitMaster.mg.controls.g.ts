import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMUnitMaster = "CMUnitMaster",
        HeaderLbl = "HeaderLbl",
        UnitIdLbl = "UnitIdLbl",
        UnitIdEdit = "UnitIdEdit",
        UnitNameLbl = "UnitNameLbl",
        UnitCodeEdit = "UnitCodeEdit",
        StatusLbl = "StatusLbl",
        ActiveCheckBox = "ActiveCheckBox",
        BtnSave = "BtnSave",
        BtnCancel = "BtnCancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get UnitIdEdit(): FormControl {
        return this.fg.controls[MgControlName.UnitIdEdit] as FormControl;
    }

    get UnitCodeEdit(): FormControl {
        return this.fg.controls[MgControlName.UnitCodeEdit] as FormControl;
    }

    get ActiveCheckBox(): FormControl {
        return this.fg.controls[MgControlName.ActiveCheckBox] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}