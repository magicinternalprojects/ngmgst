import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Details = "Details",
        Table2097155 = "Table2097155",
        Column2097156 = "Column2097156",
        CountryName = "CountryName",
        Column2097157 = "Column2097157",
        ShortName = "ShortName",
        Column2097158 = "Column2097158",
        Active = "Active",
        Label2097153 = "Label2097153",
        gNote = "gNote",
        btnAdd = "btnAdd",
        btnEdit = "btnEdit",
        btnView = "btnView",
        btnDelete = "btnDelete",
        btnClose = "btnClose",
        Column15 = "Column15",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column2097156',
        'Column2097157',
        'Column2097158',
        'Column15',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get CountryName(): FormControl {
        return this.getTableChildFormControl(MgControlName.CountryName);
    }

    get ShortName(): FormControl {
        return this.getTableChildFormControl(MgControlName.ShortName);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    get gNote(): FormControl {
        return this.fg.controls[MgControlName.gNote] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}