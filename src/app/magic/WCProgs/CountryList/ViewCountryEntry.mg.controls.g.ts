import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    ViewCountryEntry = "ViewCountryEntry",
        Search = "Search",
        vSearchText = "vSearchText",
        vSearchBtn = "vSearchBtn",
        vResetBtn = "vResetBtn",
        Details = "Details",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get vSearchText(): FormControl {
        return this.fg.controls[MgControlName.vSearchText] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}