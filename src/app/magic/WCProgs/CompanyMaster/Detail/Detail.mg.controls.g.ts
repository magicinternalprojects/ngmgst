import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        New = "New",
        Help = "Help",
        Table1048580 = "Table1048580",
        Column1 = "Column1",
        CompFullName = "CompFullName",
        Column2 = "Column2",
        ShortName = "ShortName",
        Column3 = "Column3",
        Active = "Active",
        Column4 = "Column4",
        FullName = "FullName",
        Column5 = "Column5",
        CreatedDate = "CreatedDate",
        Column6 = "Column6",
        Edit = "Edit",
        View_Detail = "View_Detail",
        Label1048578 = "Label1048578",
        gNote = "gNote",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column1',
        'Column2',
        'Column3',
        'Column4',
        'Column5',
        'Column6',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get CompFullName(): FormControl {
        return this.getTableChildFormControl(MgControlName.CompFullName);
    }

    get ShortName(): FormControl {
        return this.getTableChildFormControl(MgControlName.ShortName);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    get FullName(): FormControl {
        return this.getTableChildFormControl(MgControlName.FullName);
    }

    get CreatedDate(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDate);
    }

    get gNote(): FormControl {
        return this.fg.controls[MgControlName.gNote] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}