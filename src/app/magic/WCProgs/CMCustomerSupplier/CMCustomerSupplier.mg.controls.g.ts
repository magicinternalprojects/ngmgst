import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMCustomerSupplier = "CMCustomerSupplier",
        Save = "Save",
        Cancel = "Cancel",
        vTab = "vTab",
        Label1048582 = "Label1048582",
        Name = "Name",
        Label1048580 = "Label1048580",
        BranchName = "BranchName",
        Label1048584 = "Label1048584",
        PartyType = "PartyType",
        Label1048585 = "Label1048585",
        Active = "Active",
        Label1048586 = "Label1048586",
        RegisterType = "RegisterType",
        Label1048592 = "Label1048592",
        Address = "Address",
        Label1048594 = "Label1048594",
        CountryId = "CountryId",
        Label1048595 = "Label1048595",
        Combo_Box = "Combo_Box",
        Label1048596 = "Label1048596",
        Pincode = "Pincode",
        Label1048598 = "Label1048598",
        ContactName = "ContactName",
        Label1048600 = "Label1048600",
        ContactMobileNo = "ContactMobileNo",
        Label1048602 = "Label1048602",
        MobileNo = "MobileNo",
        Label1048604 = "Label1048604",
        Email = "Email",
        Label1048609 = "Label1048609",
        PANno = "PANno",
        Label1048611 = "Label1048611",
        BankName = "BankName",
        Label1048613 = "Label1048613",
        GSTno = "GSTno",
        Label1048615 = "Label1048615",
        AccountNo = "AccountNo",
        Label1048617 = "Label1048617",
        IFSCcode = "IFSCcode",
}
export enum MgCustomProperties {
    CMCustomerSupplier_FormName = 'CMCustomerSupplier~FormName',
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get Name(): FormControl {
        return this.fg.controls[MgControlName.Name] as FormControl;
    }

    get BranchName(): FormControl {
        return this.fg.controls[MgControlName.BranchName] as FormControl;
    }

    get PartyType(): FormControl {
        return this.fg.controls[MgControlName.PartyType] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    get RegisterType(): FormControl {
        return this.fg.controls[MgControlName.RegisterType] as FormControl;
    }

    get Address(): FormControl {
        return this.fg.controls[MgControlName.Address] as FormControl;
    }

    get CountryId(): FormControl {
        return this.fg.controls[MgControlName.CountryId] as FormControl;
    }

    get Combo_Box(): FormControl {
        return this.fg.controls[MgControlName.Combo_Box] as FormControl;
    }

    get Pincode(): FormControl {
        return this.fg.controls[MgControlName.Pincode] as FormControl;
    }

    get ContactName(): FormControl {
        return this.fg.controls[MgControlName.ContactName] as FormControl;
    }

    get ContactMobileNo(): FormControl {
        return this.fg.controls[MgControlName.ContactMobileNo] as FormControl;
    }

    get MobileNo(): FormControl {
        return this.fg.controls[MgControlName.MobileNo] as FormControl;
    }

    get Email(): FormControl {
        return this.fg.controls[MgControlName.Email] as FormControl;
    }

    get PANno(): FormControl {
        return this.fg.controls[MgControlName.PANno] as FormControl;
    }

    get BankName(): FormControl {
        return this.fg.controls[MgControlName.BankName] as FormControl;
    }

    get GSTno(): FormControl {
        return this.fg.controls[MgControlName.GSTno] as FormControl;
    }

    get AccountNo(): FormControl {
        return this.fg.controls[MgControlName.AccountNo] as FormControl;
    }

    get IFSCcode(): FormControl {
        return this.fg.controls[MgControlName.IFSCcode] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}