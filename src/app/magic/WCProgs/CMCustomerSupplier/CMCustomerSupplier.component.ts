import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMCustomerSupplier.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMCustomerSupplier',
    providers: [...magicProviders],
    styleUrls: ['./CMCustomerSupplier.component.css'],
    templateUrl: './CMCustomerSupplier.component.html'
})
export class CMCustomerSupplier extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "Customer Supplier";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "80%";
    private static readonly height: string = "550px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CMCustomerSupplier.x;
    }
    Y() {
        return CMCustomerSupplier.y;
    }
    Width(): string {
        return CMCustomerSupplier.width;
    }
    Height(): string {
        return CMCustomerSupplier.height;
    }
    IsCenteredToWindow() {
        return CMCustomerSupplier.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CMCustomerSupplier', 'FormName'));
        // return CVCustomerServicesDetails.formName;
    }
    ShowTitleBar() {
        return CMCustomerSupplier.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMCustomerSupplier.shouldCloseOnBackgroundClick;
    }
}