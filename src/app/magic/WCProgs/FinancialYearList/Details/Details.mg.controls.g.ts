import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Details = "Details",
        Table3 = "Table3",
        Column4 = "Column4",
        FyearIdEdit = "FyearIdEdit",
        Column5 = "Column5",
        CompFullNameEdit = "CompFullNameEdit",
        Column6 = "Column6",
        BranchNameEdit = "BranchNameEdit",
        Column7 = "Column7",
        FyyearEdit = "FyyearEdit",
        Column8 = "Column8",
        StartDateEdit = "StartDateEdit",
        Column9 = "Column9",
        EndDateEdit = "EndDateEdit",
        Column10 = "Column10",
        DefaultYearCheckBox = "DefaultYearCheckBox",
        Column11 = "Column11",
        CreatedByFullNameEdit = "CreatedByFullNameEdit",
        Column12 = "Column12",
        CreatedDateEdit = "CreatedDateEdit",
        F5Lbl = "F5Lbl",
        gNoteEdit = "gNoteEdit",
        btnAdd = "btnAdd",
        btnEdit = "btnEdit",
        btnView = "btnView",
        btnClose = "btnClose",
        Column26 = "Column26",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column4',
        'Column5',
        'Column6',
        'Column7',
        'Column8',
        'Column9',
        'Column10',
        'Column11',
        'Column12',
        'Column26',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get FyearIdEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.FyearIdEdit);
    }

    get CompFullNameEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CompFullNameEdit);
    }

    get BranchNameEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.BranchNameEdit);
    }

    get FyyearEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.FyyearEdit);
    }

    get StartDateEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.StartDateEdit);
    }

    get EndDateEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.EndDateEdit);
    }

    get DefaultYearCheckBox(): FormControl {
        return this.getTableChildFormControl(MgControlName.DefaultYearCheckBox);
    }

    get CreatedByFullNameEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedByFullNameEdit);
    }

    get CreatedDateEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDateEdit);
    }

    get gNoteEdit(): FormControl {
        return this.fg.controls[MgControlName.gNoteEdit] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}