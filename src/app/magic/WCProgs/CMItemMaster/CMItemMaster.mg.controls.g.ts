import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMItemMaster = "CMItemMaster",
        Label1048580 = "Label1048580",
        ItemCode = "ItemCode",
        IsService = "IsService",
        Label1048579 = "Label1048579",
        Active = "Active",
        Label1048582 = "Label1048582",
        Description = "Description",
        Label1048596 = "Label1048596",
        vHSNCode = "vHSNCode",
        btnHSN = "btnHSN",
        Label1048599 = "Label1048599",
        vHSNType = "vHSNType",
        Label1048584 = "Label1048584",
        Name = "Name",
        Label1048600 = "Label1048600",
        vCGSTId = "vCGSTId",
        Label1048586 = "Label1048586",
        Combo_Box = "Combo_Box",
        Label1048601 = "Label1048601",
        vSGSTId = "vSGSTId",
        Label1048587 = "Label1048587",
        PurchaseRate = "PurchaseRate",
        Label1048602 = "Label1048602",
        vIGSTId = "vIGSTId",
        Label1048589 = "Label1048589",
        SaleRate = "SaleRate",
        Label1048603 = "Label1048603",
        vCESSId = "vCESSId",
        Label1048591 = "Label1048591",
        TaxInclude = "TaxInclude",
        Save = "Save",
        Cancel = "Cancel",
}
export enum MgCustomProperties {
    CMItemMaster_FormName = 'CMItemMaster~FormName',
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get ItemCode(): FormControl {
        return this.fg.controls[MgControlName.ItemCode] as FormControl;
    }

    get IsService(): FormControl {
        return this.fg.controls[MgControlName.IsService] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    get Description(): FormControl {
        return this.fg.controls[MgControlName.Description] as FormControl;
    }

    get vHSNCode(): FormControl {
        return this.fg.controls[MgControlName.vHSNCode] as FormControl;
    }

    get vHSNType(): FormControl {
        return this.fg.controls[MgControlName.vHSNType] as FormControl;
    }

    get Name(): FormControl {
        return this.fg.controls[MgControlName.Name] as FormControl;
    }

    get vCGSTId(): FormControl {
        return this.fg.controls[MgControlName.vCGSTId] as FormControl;
    }

    get Combo_Box(): FormControl {
        return this.fg.controls[MgControlName.Combo_Box] as FormControl;
    }

    get vSGSTId(): FormControl {
        return this.fg.controls[MgControlName.vSGSTId] as FormControl;
    }

    get PurchaseRate(): FormControl {
        return this.fg.controls[MgControlName.PurchaseRate] as FormControl;
    }

    get vIGSTId(): FormControl {
        return this.fg.controls[MgControlName.vIGSTId] as FormControl;
    }

    get SaleRate(): FormControl {
        return this.fg.controls[MgControlName.SaleRate] as FormControl;
    }

    get vCESSId(): FormControl {
        return this.fg.controls[MgControlName.vCESSId] as FormControl;
    }

    get TaxInclude(): FormControl {
        return this.fg.controls[MgControlName.TaxInclude] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}