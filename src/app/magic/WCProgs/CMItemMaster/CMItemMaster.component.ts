import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMItemMaster.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMItemMaster',
    providers: [...magicProviders],
    templateUrl: './CMItemMaster.component.html'
})
export class CMItemMaster extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMItemMaster";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "80%";
    private static readonly height: string = "500px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CMItemMaster.x;
    }
    Y() {
        return CMItemMaster.y;
    }
    Width(): string {
        return CMItemMaster.width;
    }
    Height(): string {
        return CMItemMaster.height;
    }
    IsCenteredToWindow() {
        return CMItemMaster.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CMItemMaster', 'FormName'));
        // return CVCustomerServicesDetails.formName;
    }
    ShowTitleBar() {
        return CMItemMaster.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMItemMaster.shouldCloseOnBackgroundClick;
    }
}