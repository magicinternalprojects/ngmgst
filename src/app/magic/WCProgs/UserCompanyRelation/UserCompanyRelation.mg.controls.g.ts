import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    UserCompanyRelation = "UserCompanyRelation",
        Group15 = "Group15",
        Label3 = "Label3",
        PersonalEmail = "PersonalEmail",
        MobileNumber = "MobileNumber",
        Edit9 = "Edit9",
        btnEdit_Profile = "btnEdit_Profile",
        CompanyList = "CompanyList",
        Create_company = "Create_company",
        Create_Branch = "Create_Branch",
        Need_help = "Need_help",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get PersonalEmail(): FormControl {
        return this.fg.controls[MgControlName.PersonalEmail] as FormControl;
    }

    get MobileNumber(): FormControl {
        return this.fg.controls[MgControlName.MobileNumber] as FormControl;
    }

    get Edit9(): FormControl {
        return this.fg.controls[MgControlName.Edit9] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}