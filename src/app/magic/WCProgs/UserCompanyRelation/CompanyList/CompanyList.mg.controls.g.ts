import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CompanyList = "CompanyList",
        Table3 = "Table3",
        Column4 = "Column4",
        BranchName = "BranchName",
        Column5 = "Column5",
        BranchLocation = "BranchLocation",
        Column6 = "Column6",
        Status = "Status",
        Label1 = "Label1",
        Go = "Go",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column4',
        'Column5',
        'Column6',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get BranchName(): FormControl {
        return this.getTableChildFormControl(MgControlName.BranchName);
    }

    get BranchLocation(): FormControl {
        return this.getTableChildFormControl(MgControlName.BranchLocation);
    }

    get Status(): FormControl {
        return this.getTableChildFormControl(MgControlName.Status);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}