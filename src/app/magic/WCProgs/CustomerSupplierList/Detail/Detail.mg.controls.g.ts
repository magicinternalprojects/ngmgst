import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        New = "New",
        Table1048580 = "Table1048580",
        Column1048581 = "Column1048581",
        CustName = "CustName",
        Column1048582 = "Column1048582",
        PartyType = "PartyType",
        Column1048583 = "Column1048583",
        MobileNo = "MobileNo",
        Column1048584 = "Column1048584",
        GSTno = "GSTno",
        Column1048585 = "Column1048585",
        Active = "Active",
        Column1048586 = "Column1048586",
        FullName = "FullName",
        Column1048587 = "Column1048587",
        CreatedDate = "CreatedDate",
        Column1048600 = "Column1048600",
        Edit = "Edit",
        View_Detail = "View_Detail",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column1048581',
        'Column1048582',
        'Column1048583',
        'Column1048584',
        'Column1048585',
        'Column1048586',
        'Column1048587',
        'Column1048600',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get CustName(): FormControl {
        return this.getTableChildFormControl(MgControlName.CustName);
    }

    get PartyType(): FormControl {
        return this.getTableChildFormControl(MgControlName.PartyType);
    }

    get MobileNo(): FormControl {
        return this.getTableChildFormControl(MgControlName.MobileNo);
    }

    get GSTno(): FormControl {
        return this.getTableChildFormControl(MgControlName.GSTno);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    get FullName(): FormControl {
        return this.getTableChildFormControl(MgControlName.FullName);
    }

    get CreatedDate(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDate);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}