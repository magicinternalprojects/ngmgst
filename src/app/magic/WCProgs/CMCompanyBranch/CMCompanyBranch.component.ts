import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMCompanyBranch.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMCompanyBranch',
    providers: [...magicProviders],
    templateUrl: './CMCompanyBranch.component.html'
})
export class CMCompanyBranch extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMCompanyBranch";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "70%";
    private static readonly height: string = "650px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CMCompanyBranch.x;
    }
    Y() {
        return CMCompanyBranch.y;
    }
    Width(): string {
        return CMCompanyBranch.width;
    }
    Height(): string {
        return CMCompanyBranch.height;
    }
    IsCenteredToWindow() {
        return CMCompanyBranch.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CMCompanyBranch', 'FormName'));
        //return CMCompanyBranch.formName;
    }
    ShowTitleBar() {
        return CMCompanyBranch.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMCompanyBranch.shouldCloseOnBackgroundClick;
    }
}