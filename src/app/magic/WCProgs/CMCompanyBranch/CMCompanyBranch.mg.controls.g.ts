import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMCompanyBranch = "CMCompanyBranch",
        L1 = "L1",
        BranchId = "BranchId",
        L2 = "L2",
        FullName = "FullName",
        L3 = "L3",
        BranchName = "BranchName",
        L4 = "L4",
        BranchCode = "BranchCode",
        L5 = "L5",
        BranchLocation = "BranchLocation",
        L6 = "L6",
        Active = "Active",
        vTab = "vTab",
        L7 = "L7",
        Address = "Address",
        L8 = "L8",
        CountryId = "CountryId",
        L9 = "L9",
        StateId = "StateId",
        L10 = "L10",
        Pincode = "Pincode",
        L11 = "L11",
        ContactName = "ContactName",
        L12 = "L12",
        ContactMobileNo = "ContactMobileNo",
        L13 = "L13",
        MobileNo = "MobileNo",
        L14 = "L14",
        Email = "Email",
        Statutory_Details = "Statutory_Details",
        L15 = "L15",
        PANno = "PANno",
        L17 = "L17",
        BankName = "BankName",
        L16 = "L16",
        GSTNo = "GSTNo",
        L18 = "L18",
        AccountNo = "AccountNo",
        L19 = "L19",
        IFSCcode = "IFSCcode",
        Save = "Save",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get BranchId(): FormControl {
        return this.fg.controls[MgControlName.BranchId] as FormControl;
    }

    get FullName(): FormControl {
        return this.fg.controls[MgControlName.FullName] as FormControl;
    }

    get BranchName(): FormControl {
        return this.fg.controls[MgControlName.BranchName] as FormControl;
    }

    get BranchCode(): FormControl {
        return this.fg.controls[MgControlName.BranchCode] as FormControl;
    }

    get BranchLocation(): FormControl {
        return this.fg.controls[MgControlName.BranchLocation] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    get Address(): FormControl {
        return this.fg.controls[MgControlName.Address] as FormControl;
    }

    get CountryId(): FormControl {
        return this.fg.controls[MgControlName.CountryId] as FormControl;
    }

    get StateId(): FormControl {
        return this.fg.controls[MgControlName.StateId] as FormControl;
    }

    get Pincode(): FormControl {
        return this.fg.controls[MgControlName.Pincode] as FormControl;
    }

    get ContactName(): FormControl {
        return this.fg.controls[MgControlName.ContactName] as FormControl;
    }

    get ContactMobileNo(): FormControl {
        return this.fg.controls[MgControlName.ContactMobileNo] as FormControl;
    }

    get MobileNo(): FormControl {
        return this.fg.controls[MgControlName.MobileNo] as FormControl;
    }

    get Email(): FormControl {
        return this.fg.controls[MgControlName.Email] as FormControl;
    }

    get PANno(): FormControl {
        return this.fg.controls[MgControlName.PANno] as FormControl;
    }

    get BankName(): FormControl {
        return this.fg.controls[MgControlName.BankName] as FormControl;
    }

    get GSTNo(): FormControl {
        return this.fg.controls[MgControlName.GSTNo] as FormControl;
    }

    get AccountNo(): FormControl {
        return this.fg.controls[MgControlName.AccountNo] as FormControl;
    }

    get IFSCcode(): FormControl {
        return this.fg.controls[MgControlName.IFSCcode] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}