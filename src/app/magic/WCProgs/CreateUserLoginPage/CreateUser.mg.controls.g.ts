import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CreateUser = "CreateUser",
        Label1048577 = "Label1048577",
        Label1048578 = "Label1048578",
        FirstName1 = "FirstName1",
        Label1048580 = "Label1048580",
        LastName = "LastName",
        Label1048582 = "Label1048582",
        UserName = "UserName",
        Label1048584 = "Label1048584",
        Password = "Password",
        Label1048588 = "Label1048588",
        OfficialEmail = "OfficialEmail",
        Label1048590 = "Label1048590",
        PersonalEmail = "PersonalEmail",
        Label1048594 = "Label1048594",
        ContactNumber = "ContactNumber",
        Label1048592 = "Label1048592",
        MobileNumber = "MobileNumber",
        Label1048586 = "Label1048586",
        vConfirmPassword = "vConfirmPassword",
        Save = "Save",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get FirstName1(): FormControl {
        return this.fg.controls[MgControlName.FirstName1] as FormControl;
    }

    get LastName(): FormControl {
        return this.fg.controls[MgControlName.LastName] as FormControl;
    }

    get UserName(): FormControl {
        return this.fg.controls[MgControlName.UserName] as FormControl;
    }

    get Password(): FormControl {
        return this.fg.controls[MgControlName.Password] as FormControl;
    }

    get OfficialEmail(): FormControl {
        return this.fg.controls[MgControlName.OfficialEmail] as FormControl;
    }

    get PersonalEmail(): FormControl {
        return this.fg.controls[MgControlName.PersonalEmail] as FormControl;
    }

    get ContactNumber(): FormControl {
        return this.fg.controls[MgControlName.ContactNumber] as FormControl;
    }

    get MobileNumber(): FormControl {
        return this.fg.controls[MgControlName.MobileNumber] as FormControl;
    }

    get vConfirmPassword(): FormControl {
        return this.fg.controls[MgControlName.vConfirmPassword] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}