import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CreateUser.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CreateUser',
    providers: [...magicProviders],
    templateUrl: './CreateUser.component.html'
})
export class CreateUser extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CreateUser";
    private static readonly showTitleBar: boolean = true;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "300px";
    private static readonly height: string = "300px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;
    X() {
        return CreateUser.x;
    }
    Y() {
        return CreateUser.y;
    }
    Width(): string {
        return CreateUser.width;
    }
    Height(): string {
        return CreateUser.height;
    }
    IsCenteredToWindow() {
        return CreateUser.isCenteredToWindow;
    }
    FormName() {
        return CreateUser.formName;
    }
    ShowTitleBar() {
        return CreateUser.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CreateUser.shouldCloseOnBackgroundClick;
    }
}