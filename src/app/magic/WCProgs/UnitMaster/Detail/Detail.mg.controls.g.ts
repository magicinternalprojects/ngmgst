import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        Table4 = "Table4",
        Column5 = "Column5",
        UnitIdEdit = "UnitIdEdit",
        Column6 = "Column6",
        UnitCodeEdit = "UnitCodeEdit",
        Column7 = "Column7",
        ActiveCheckBox = "ActiveCheckBox",
        Column8 = "Column8",
        CreatedByFullNameEdit = "CreatedByFullNameEdit",
        Column9 = "Column9",
        CreatedDateEdit = "CreatedDateEdit",
        F5Lbl = "F5Lbl",
        gNoteLbl = "gNoteLbl",
        BtnNew = "BtnNew",
        BtnView_Detail = "BtnView_Detail",
        BtnEdit = "BtnEdit",
        BtnHelp = "BtnHelp",
        BtnClose = "BtnClose",
        Column20 = "Column20",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column5',
        'Column6',
        'Column7',
        'Column8',
        'Column9',
        'Column20',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get UnitIdEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.UnitIdEdit);
    }

    get UnitCodeEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.UnitCodeEdit);
    }

    get ActiveCheckBox(): FormControl {
        return this.getTableChildFormControl(MgControlName.ActiveCheckBox);
    }

    get CreatedByFullNameEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedByFullNameEdit);
    }

    get CreatedDateEdit(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDateEdit);
    }

    get gNoteLbl(): FormControl {
        return this.fg.controls[MgControlName.gNoteLbl] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}