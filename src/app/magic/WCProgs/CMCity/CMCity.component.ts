import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMCity.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMCity',
    providers: [...magicProviders],
    templateUrl: './CMCity.component.html'
})
export class CMCity extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMCity";
    private static readonly showTitleBar: boolean = true;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "300px";
    private static readonly height: string = "300px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;
    X() {
        return CMCity.x;
    }
    Y() {
        return CMCity.y;
    }
    Width(): string {
        return CMCity.width;
    }
    Height(): string {
        return CMCity.height;
    }
    IsCenteredToWindow() {
        return CMCity.isCenteredToWindow;
    }
    FormName() {
        return CMCity.formName;
    }
    ShowTitleBar() {
        return CMCity.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMCity.shouldCloseOnBackgroundClick;
    }
}