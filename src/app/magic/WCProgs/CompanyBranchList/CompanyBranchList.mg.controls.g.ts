import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CompanyBranchList = "CompanyBranchList",
        L1 = "L1",
        vSearchText = "vSearchText",
        Search = "Search",
        Reset = "Reset",
        Detail = "Detail",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get vSearchText(): FormControl {
        return this.fg.controls[MgControlName.vSearchText] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}