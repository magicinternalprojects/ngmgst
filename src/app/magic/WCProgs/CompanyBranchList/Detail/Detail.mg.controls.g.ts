import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        Table2097155 = "Table2097155",
        Column2097156 = "Column2097156",
        BranchName = "BranchName",
        Column2097157 = "Column2097157",
        BranchLocation = "BranchLocation",
        Column2097158 = "Column2097158",
        GSTNo = "GSTNo",
        Column2097159 = "Column2097159",
        Address = "Address",
        Column2097160 = "Column2097160",
        Active = "Active",
        Column2097161 = "Column2097161",
        FullName = "FullName",
        Column2097162 = "Column2097162",
        CreatedDate = "CreatedDate",
        F5 = "F5",
        gNote = "gNote",
        New = "New",
        View_Detail = "View_Detail",
        Edit = "Edit",
        Help = "Help",
        Close = "Close",
        Column15="Column15"
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column2097156',
        'Column2097157',
        'Column2097158',
        'Column2097159',
        'Column2097160',
        'Column15',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get BranchName(): FormControl {
        return this.getTableChildFormControl(MgControlName.BranchName);
    }

    get BranchLocation(): FormControl {
        return this.getTableChildFormControl(MgControlName.BranchLocation);
    }

    get GSTNo(): FormControl {
        return this.getTableChildFormControl(MgControlName.GSTNo);
    }

    get Address(): FormControl {
        return this.getTableChildFormControl(MgControlName.Address);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    get FullName(): FormControl {
        return this.getTableChildFormControl(MgControlName.FullName);
    }

    get CreatedDate(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDate);
    }

    get gNote(): FormControl {
        return this.fg.controls[MgControlName.gNote] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}