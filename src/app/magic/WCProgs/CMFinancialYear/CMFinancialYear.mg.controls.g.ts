import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMFinancialYear = "CMFinancialYear",
        HeaderLbl = "HeaderLbl",
        BranchLbl = "BranchLbl",
        BranchNameEdit = "BranchNameEdit",
        FinYearLbl = "FinYearLbl",
        FyyearEdit = "FyyearEdit",
        StartDateLbl = "StartDateLbl",
        StartDateEdit = "StartDateEdit",
        EndDateLbl = "EndDateLbl",
        EndDateEdit = "EndDateEdit",
        DefaultYearLbl = "DefaultYearLbl",
        DefaultYearCheckBox = "DefaultYearCheckBox",
        BtnSave = "BtnSave",
        BtnCancel = "BtnCancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get BranchNameEdit(): FormControl {
        return this.fg.controls[MgControlName.BranchNameEdit] as FormControl;
    }

    get FyyearEdit(): FormControl {
        return this.fg.controls[MgControlName.FyyearEdit] as FormControl;
    }

    get StartDateEdit(): FormControl {
        return this.fg.controls[MgControlName.StartDateEdit] as FormControl;
    }

    get EndDateEdit(): FormControl {
        return this.fg.controls[MgControlName.EndDateEdit] as FormControl;
    }

    get DefaultYearCheckBox(): FormControl {
        return this.fg.controls[MgControlName.DefaultYearCheckBox] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}