import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMFinancialYear.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMFinancialYear',
    providers: [...magicProviders],
    templateUrl: './CMFinancialYear.component.html'
})
export class CMFinancialYear extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMFinancialYear";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "50%";
    private static readonly height: string = "350px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;
    X() {
        return CMFinancialYear.x;
    }
    Y() {
        return CMFinancialYear.y;
    }
    Width(): string {
        return CMFinancialYear.width;
    }
    Height(): string {
        return CMFinancialYear.height;
    }
    IsCenteredToWindow() {
        return CMFinancialYear.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CMFinancialYear', 'FormName'));
        //return CMFinancialYear.formName;
    }
    ShowTitleBar() {
        return CMFinancialYear.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMFinancialYear.shouldCloseOnBackgroundClick;
    }
}