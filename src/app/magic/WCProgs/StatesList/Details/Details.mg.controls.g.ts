import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Details = "Details",
        Table2097154 = "Table2097154",
        Column2097155 = "Column2097155",
        StateName = "StateName",
        Column2097156 = "Column2097156",
        StateCode = "StateCode",
        Column2097157 = "Column2097157",
        CName = "CName",
        Column2097158 = "Column2097158",
        ShortName = "ShortName",
        Column2097159 = "Column2097159",
        Active = "Active",
        gNote = "gNote",
        btnAdd = "btnAdd",
        btnEdit = "btnEdit",
        btnView = "btnView",
        btnDelete = "btnDelete",
        btnClose = "btnClose",
        Column15="Column15",
        Column2097160="Column2097160",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column2097155',
        'Column2097156',
        'Column2097157',
        'Column2097158',
        'Column2097159',
        'Column2097160',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get StateName(): FormControl {
        return this.getTableChildFormControl(MgControlName.StateName);
    }

    get StateCode(): FormControl {
        return this.getTableChildFormControl(MgControlName.StateCode);
    }

    get CName(): FormControl {
        return this.getTableChildFormControl(MgControlName.CName);
    }

    get ShortName(): FormControl {
        return this.getTableChildFormControl(MgControlName.ShortName);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    get gNote(): FormControl {
        return this.fg.controls[MgControlName.gNote] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}