import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMTaxMaster = "CMTaxMaster",
        HeaderLbl = "HeaderLbl",
        TaxIdLbl = "TaxIdLbl",
        TaxIdEdit = "TaxIdEdit",
        TaxTypeLbl = "TaxTypeLbl",
        TaxTypeCombo = "TaxTypeCombo",
        TaxPercentageLbl = "TaxPercentageLbl",
        PurchaseRateEdit = "PurchaseRateEdit",
        DescriptionLbl = "DescriptionLbl",
        DescriptionEdit = "DescriptionEdit",
        StatusLbl = "StatusLbl",
        StatusActiveCheckBox = "StatusActiveCheckBox",
        BtnSave = "BtnSave",
        BtnCancel = "BtnCancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get TaxIdEdit(): FormControl {
        return this.fg.controls[MgControlName.TaxIdEdit] as FormControl;
    }

    get TaxTypeCombo(): FormControl {
        return this.fg.controls[MgControlName.TaxTypeCombo] as FormControl;
    }

    get PurchaseRateEdit(): FormControl {
        return this.fg.controls[MgControlName.PurchaseRateEdit] as FormControl;
    }

    get DescriptionEdit(): FormControl {
        return this.fg.controls[MgControlName.DescriptionEdit] as FormControl;
    }

    get StatusActiveCheckBox(): FormControl {
        return this.fg.controls[MgControlName.StatusActiveCheckBox] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}