import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CTaxType = "CTaxType",
        TaxTypeLbl = "TaxTypeLbl",
        TaxTypeEdit = "TaxTypeEdit",
        BtnSave = "BtnSave",
        BtnCancel = "BtnCancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get TaxTypeEdit(): FormControl {
        return this.fg.controls[MgControlName.TaxTypeEdit] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}