import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CTaxType.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CTaxType',
    providers: [...magicProviders],
    templateUrl: './CTaxType.component.html'
})
export class CTaxType extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CTaxType";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "50%";
    private static readonly height: string = "300px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;
    X() {
        return CTaxType.x;
    }
    Y() {
        return CTaxType.y;
    }
    Width(): string {
        return CTaxType.width;
    }
    Height(): string {
        return CTaxType.height;
    }
    IsCenteredToWindow() {
        return CTaxType.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CTaxType', 'FormName'));
        //return CTaxType.formName;
    }
    ShowTitleBar() {
        return CTaxType.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CTaxType.shouldCloseOnBackgroundClick;
    }
}