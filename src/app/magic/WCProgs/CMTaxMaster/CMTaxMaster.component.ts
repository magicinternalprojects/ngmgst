import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMTaxMaster.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMTaxMaster',
    providers: [...magicProviders],
    templateUrl: './CMTaxMaster.component.html'
})
export class CMTaxMaster extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMTaxMaster";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "50%";
    private static readonly height: string = "300px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;
    X() {
        return CMTaxMaster.x;
    }
    Y() {
        return CMTaxMaster.y;
    }
    Width(): string {
        return CMTaxMaster.width;
    }
    Height(): string {
        return CMTaxMaster.height;
    }
    IsCenteredToWindow() {
        return CMTaxMaster.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CMTaxMaster', 'FormName'));
        //return CMTaxMaster.formName;
    }
    ShowTitleBar() {
        return CMTaxMaster.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMTaxMaster.shouldCloseOnBackgroundClick;
    }
}