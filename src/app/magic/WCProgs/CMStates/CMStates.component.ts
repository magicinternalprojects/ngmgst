import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMStates.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMStates',
    providers: [...magicProviders],
    templateUrl: './CMStates.component.html'
})
export class CMStates extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMStates";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "460px";
    private static readonly height: string = "80vh";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CMStates.x;
    }
    Y() {
        return CMStates.y;
    }
    Width(): string {
        return CMStates.width;
    }
    Height(): string {
        return CMStates.height;
    }
    IsCenteredToWindow() {
        return CMStates.isCenteredToWindow;
    }
    FormName() {
        return (this.mg.getCustomProperty ('CMStates', 'FormName'));
        //return CMStates.formName;
    }
    ShowTitleBar() {
        return CMStates.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMStates.shouldCloseOnBackgroundClick;
    }
}