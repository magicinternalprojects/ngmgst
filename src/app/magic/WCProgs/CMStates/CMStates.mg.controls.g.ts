import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMStates = "CMStates",
        L1 = "L1",
        CountryName = "CountryName",
        btnCountry = "btnCountry",
        L2 = "L2",
        StateName = "StateName",
        L3 = "L3",
        ShortName = "ShortName",
        L4 = "L4",
        StateCode = "StateCode",
        L5 = "L5",
        Active = "Active",
        Save = "Save",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get CountryName(): FormControl {
        return this.fg.controls[MgControlName.CountryName] as FormControl;
    }

    get StateName(): FormControl {
        return this.fg.controls[MgControlName.StateName] as FormControl;
    }

    get ShortName(): FormControl {
        return this.fg.controls[MgControlName.ShortName] as FormControl;
    }

    get StateCode(): FormControl {
        return this.fg.controls[MgControlName.StateCode] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}