import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./Home.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";

declare function ReInit(): void;
@Component({
    selector: 'mga-Home',
    providers: [...magicProviders],
    templateUrl: './Home.component.html'
})
export class Home extends TaskBaseMagicComponent {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }

    
    ngAfterViewInit()
    {
      super.ngAfterViewInit();
       ReInit();
    }
}