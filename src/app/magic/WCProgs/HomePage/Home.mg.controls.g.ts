import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Home = "Home",
        Profile = "Profile",
        Logout = "Logout",
        MainCanvas = "MainCanvas",
}
export enum MgCustomProperties {
    Home_User = 'Home~User',
        Home_SuperUser = 'Home~SuperUser',
        Home_Menu = 'Home~Menu',
        Home_ProfilePic = 'Home~ProfilePic',
        Home_AppName = 'Home~AppName',
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}