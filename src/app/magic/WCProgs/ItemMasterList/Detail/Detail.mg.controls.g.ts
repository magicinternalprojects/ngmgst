import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        New = "New",
        Table1048577 = "Table1048577",
        Column1048578 = "Column1048578",
        ItemCode = "ItemCode",
        Column1048579 = "Column1048579",
        Description = "Description",
        Column1048580 = "Column1048580",
        Mcode = "Mcode",
        Column1048581 = "Column1048581",
        CompFullName = "CompFullName",
        Column1048582 = "Column1048582",
        HSNNo = "HSNNo",
        Column1048583 = "Column1048583",
        TaxInclude = "TaxInclude",
        Column1048584 = "Column1048584",
        Active = "Active",
        Column1048585 = "Column1048585",
        FullName = "FullName",
        Column1048586 = "Column1048586",
        CreatedDate = "CreatedDate",
        Column1048601 = "Column1048601",
        Edit = "Edit",
        View_Detail = "View_Detail",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column1048578',
        'Column1048579',
        'Column1048580',
        'Column1048581',
        'Column1048582',
        'Column1048583',
        'Column1048584',
        'Column1048585',
        'Column1048586',
        'Column1048601',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get ItemCode(): FormControl {
        return this.getTableChildFormControl(MgControlName.ItemCode);
    }

    get Description(): FormControl {
        return this.getTableChildFormControl(MgControlName.Description);
    }

    get Mcode(): FormControl {
        return this.getTableChildFormControl(MgControlName.Mcode);
    }

    get CompFullName(): FormControl {
        return this.getTableChildFormControl(MgControlName.CompFullName);
    }

    get HSNNo(): FormControl {
        return this.getTableChildFormControl(MgControlName.HSNNo);
    }

    get TaxInclude(): FormControl {
        return this.getTableChildFormControl(MgControlName.TaxInclude);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    get FullName(): FormControl {
        return this.getTableChildFormControl(MgControlName.FullName);
    }

    get CreatedDate(): FormControl {
        return this.getTableChildFormControl(MgControlName.CreatedDate);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}