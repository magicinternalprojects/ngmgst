import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CompanyCM.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CompanyCM',
    providers: [...magicProviders],
    styleUrls: ['./CompanyCM.component.css'],
    templateUrl: './CompanyCM.component.html'
})
export class CompanyCM extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "Company CM";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "70vw";
    private static readonly height: string = "650px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CompanyCM.x;
    }
    Y() {
        return CompanyCM.y;
    }
    Width(): string {
        return CompanyCM.width;
    }
    Height(): string {
        return CompanyCM.height;
    }
    IsCenteredToWindow() {
        return CompanyCM.isCenteredToWindow;
    }
    FormName() {
        return CompanyCM.formName;
    }
    ShowTitleBar() {
        return CompanyCM.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CompanyCM.shouldCloseOnBackgroundClick;
    }
}