import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CompanyCM = "CompanyCM",
        Save = "Save",
        Cancel = "Cancel",
        Label1048579 = "Label1048579",
        Label1048581 = "Label1048581",
        Label1048582 = "Label1048582",
        Label1048584 = "Label1048584",
        FullName = "FullName",
        ShortName = "ShortName",
        Location = "Location",
        Label1048586 = "Label1048586",
        Label1048589 = "Label1048589",
        ShareItem = "ShareItem",
        BranchWiseBilling = "BranchWiseBilling",
        Label1048627 = "Label1048627",
        Label1048587 = "Label1048587",
        Label1048629 = "Label1048629",
        InvoicePrefix = "InvoicePrefix",
        ShareCustomer = "ShareCustomer",
        PrintFormatNo = "PrintFormatNo",
        Label1048588 = "Label1048588",
        Active = "Active",
        vTab = "vTab",
        Label1048597 = "Label1048597",
        Address = "Address",
        Label1048598 = "Label1048598",
        CountryId = "CountryId",
        Label1048599 = "Label1048599",
        StateId = "StateId",
        Label1048600 = "Label1048600",
        Pincode = "Pincode",
        Label1048602 = "Label1048602",
        ContactName = "ContactName",
        Label1048604 = "Label1048604",
        ContactMobileNo = "ContactMobileNo",
        Label1048606 = "Label1048606",
        MobileNo = "MobileNo",
        Label1048609 = "Label1048609",
        Email = "Email",
        Group1048612 = "Group1048612",
        Label1048613 = "Label1048613",
        PANno = "PANno",
        Label1048615 = "Label1048615",
        BankName = "BankName",
        Label1048617 = "Label1048617",
        GSTno = "GSTno",
        Label1048619 = "Label1048619",
        AccountNo = "AccountNo",
        Label1048621 = "Label1048621",
        RegisterType = "RegisterType",
        Label1048623 = "Label1048623",
        IFSCcode = "IFSCcode",
}
export enum MgCustomProperties {
    CompanyCM_FormName = 'CompanyCM~FormName',
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get FullName(): FormControl {
        return this.fg.controls[MgControlName.FullName] as FormControl;
    }

    get ShortName(): FormControl {
        return this.fg.controls[MgControlName.ShortName] as FormControl;
    }

    get Location(): FormControl {
        return this.fg.controls[MgControlName.Location] as FormControl;
    }

    get ShareItem(): FormControl {
        return this.fg.controls[MgControlName.ShareItem] as FormControl;
    }

    get BranchWiseBilling(): FormControl {
        return this.fg.controls[MgControlName.BranchWiseBilling] as FormControl;
    }

    get InvoicePrefix(): FormControl {
        return this.fg.controls[MgControlName.InvoicePrefix] as FormControl;
    }

    get ShareCustomer(): FormControl {
        return this.fg.controls[MgControlName.ShareCustomer] as FormControl;
    }

    get PrintFormatNo(): FormControl {
        return this.fg.controls[MgControlName.PrintFormatNo] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    get Address(): FormControl {
        return this.fg.controls[MgControlName.Address] as FormControl;
    }

    get CountryId(): FormControl {
        return this.fg.controls[MgControlName.CountryId] as FormControl;
    }

    get StateId(): FormControl {
        return this.fg.controls[MgControlName.StateId] as FormControl;
    }

    get Pincode(): FormControl {
        return this.fg.controls[MgControlName.Pincode] as FormControl;
    }

    get ContactName(): FormControl {
        return this.fg.controls[MgControlName.ContactName] as FormControl;
    }

    get ContactMobileNo(): FormControl {
        return this.fg.controls[MgControlName.ContactMobileNo] as FormControl;
    }

    get MobileNo(): FormControl {
        return this.fg.controls[MgControlName.MobileNo] as FormControl;
    }

    get Email(): FormControl {
        return this.fg.controls[MgControlName.Email] as FormControl;
    }

    get PANno(): FormControl {
        return this.fg.controls[MgControlName.PANno] as FormControl;
    }

    get BankName(): FormControl {
        return this.fg.controls[MgControlName.BankName] as FormControl;
    }

    get GSTno(): FormControl {
        return this.fg.controls[MgControlName.GSTno] as FormControl;
    }

    get AccountNo(): FormControl {
        return this.fg.controls[MgControlName.AccountNo] as FormControl;
    }

    get RegisterType(): FormControl {
        return this.fg.controls[MgControlName.RegisterType] as FormControl;
    }

    get IFSCcode(): FormControl {
        return this.fg.controls[MgControlName.IFSCcode] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}