import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMCountry.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMCountry',
    providers: [...magicProviders],
    templateUrl: './CMCountry.component.html'
})
export class CMCountry extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMCountry";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "460px";
    private static readonly height: string = "58vh";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CMCountry.x;
    }
    Y() {
        return CMCountry.y;
    }
    Width(): string {
        return CMCountry.width;
    }
    Height(): string {
        return CMCountry.height;
    }
    IsCenteredToWindow() {
        return CMCountry.isCenteredToWindow;
    }
    FormName() {

        return (this.mg.getCustomProperty ('CMCountry', 'FormName'));
       // return CMCountry.formName;
    }
    ShowTitleBar() {
        return CMCountry.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMCountry.shouldCloseOnBackgroundClick;
    }
}