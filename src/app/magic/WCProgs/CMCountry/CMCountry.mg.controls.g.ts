import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMCountry = "CMCountry",
        Name = "Name",
        CountryName = "CountryName",
        Short = "Short",
        ShortName = "ShortName",
        CheckActive = "CheckActive",
        Active = "Active",
        Save = "Save",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get CountryName(): FormControl {
        return this.fg.controls[MgControlName.CountryName] as FormControl;
    }

    get ShortName(): FormControl {
        return this.fg.controls[MgControlName.ShortName] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}