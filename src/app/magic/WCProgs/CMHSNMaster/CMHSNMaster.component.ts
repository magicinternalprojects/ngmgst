import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMHSNMaster.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMHSNMaster',
    providers: [...magicProviders],
    templateUrl: './CMHSNMaster.component.html'
})
export class CMHSNMaster extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "HSN Master";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "60%";
    private static readonly height: string = "400px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;


    X() {
        return CMHSNMaster.x;
    }
    Y() {
        return CMHSNMaster.y;
    }
    Width(): string {
        return CMHSNMaster.width;
    }
    Height(): string {
        return CMHSNMaster.height;
    }
    IsCenteredToWindow() {
        return CMHSNMaster.isCenteredToWindow;
    }
    // FormName() {
    //     return CMHSNMaster.formName;
    // }
    FormName() {
        return (this.mg.getCustomProperty ('CMHSNMaster', 'FormName'));
        // return CVCustomerServicesDetails.formName;
    }
    ShowTitleBar() {
        return CMHSNMaster.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMHSNMaster.shouldCloseOnBackgroundClick;
    }
}