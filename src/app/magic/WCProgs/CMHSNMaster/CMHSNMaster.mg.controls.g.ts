import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMHSNMaster = "CMHSNMaster",
        HeaderLbl = "HeaderLbl",
        SelectTypeLbl = "SelectTypeLbl",
        HSN_SAC_Type_Combo = "HSN_SAC_Type_Combo",
        HSN_SAC_NoLbl = "HSN_SAC_NoLbl",
        HSN_SAC_CodeEdit = "HSN_SAC_CodeEdit",
        CGSTLbl = "CGSTLbl",
        PCGSTIdCombo = "PCGSTIdCombo",
        SGSTLbl = "SGSTLbl",
        PSGSTIdCombo = "PSGSTIdCombo",
        IGSTLbl = "IGSTLbl",
        PIGSTIdCombo = "PIGSTIdCombo",
        CESSLbl = "CESSLbl",
        CESSIdCombo = "CESSIdCombo",
        HSN_SAC_DescriptionLbl = "HSN_SAC_DescriptionLbl",
        HSNDisciptionTextArea = "HSNDisciptionTextArea",
        ActiveLbl = "ActiveLbl",
        ActiveCheckBox = "ActiveCheckBox",
        BtnSave = "BtnSave",
        BtnCancel = "BtnCancel",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get HSN_SAC_Type_Combo(): FormControl {
        return this.fg.controls[MgControlName.HSN_SAC_Type_Combo] as FormControl;
    }

    get HSN_SAC_CodeEdit(): FormControl {
        return this.fg.controls[MgControlName.HSN_SAC_CodeEdit] as FormControl;
    }

    get PCGSTIdCombo(): FormControl {
        return this.fg.controls[MgControlName.PCGSTIdCombo] as FormControl;
    }

    get PSGSTIdCombo(): FormControl {
        return this.fg.controls[MgControlName.PSGSTIdCombo] as FormControl;
    }

    get PIGSTIdCombo(): FormControl {
        return this.fg.controls[MgControlName.PIGSTIdCombo] as FormControl;
    }

    get CESSIdCombo(): FormControl {
        return this.fg.controls[MgControlName.CESSIdCombo] as FormControl;
    }

    get HSNDisciptionTextArea(): FormControl {
        return this.fg.controls[MgControlName.HSNDisciptionTextArea] as FormControl;
    }

    get ActiveCheckBox(): FormControl {
        return this.fg.controls[MgControlName.ActiveCheckBox] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}