import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        New = "New",
        Print = "Print",
        Email = "Email",
        Export = "Export",
        Table4 = "Table4",
        Column5 = "Column5",
        SalesCode = "SalesCode",
        Column13 = "Column13",
        LogDate = "LogDate",
        Column6 = "Column6",
        CustName = "CustName",
        Column7 = "Column7",
        RefNo = "RefNo",
        Column8 = "Column8",
        Basic = "Basic",
        Column9 = "Column9",
        Tax = "Tax",
        Column10 = "Column10",
        Total = "Total",
        Column31 = "Column31",
        Edit = "Edit",
        View_Detail = "View_Detail",
        Active = "Active",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column5',
        'Column13',
        'Column6',
        'Column7',
        'Column8',
        'Column9',
        'Column10',
        'Column31',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get SalesCode(): FormControl {
        return this.getTableChildFormControl(MgControlName.SalesCode);
    }

    get LogDate(): FormControl {
        return this.getTableChildFormControl(MgControlName.LogDate);
    }

    get CustName(): FormControl {
        return this.getTableChildFormControl(MgControlName.CustName);
    }

    get RefNo(): FormControl {
        return this.getTableChildFormControl(MgControlName.RefNo);
    }

    get Basic(): FormControl {
        return this.getTableChildFormControl(MgControlName.Basic);
    }

    get Tax(): FormControl {
        return this.getTableChildFormControl(MgControlName.Tax);
    }

    get Total(): FormControl {
        return this.getTableChildFormControl(MgControlName.Total);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}