import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMPurchaseTest.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMPurchaseTest',
    providers: [...magicProviders],
    styleUrls: ['./CMPurchase.component.css'],
    templateUrl: './CMPurchaseTest.component.html'
})
export class CMPurchaseTest extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMPurchaseTest";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "80%";
    private static readonly height: string = "700px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CMPurchaseTest.x;
    }
    Y() {
        return CMPurchaseTest.y;
    }
    Width(): string {
        return CMPurchaseTest.width;
    }
    Height(): string {
        return CMPurchaseTest.height;
    }
    IsCenteredToWindow() {
        return CMPurchaseTest.isCenteredToWindow;
    }
    FormName() {
        return CMPurchaseTest.formName;
    }
    ShowTitleBar() {
        return CMPurchaseTest.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMPurchaseTest.shouldCloseOnBackgroundClick;
    }
}