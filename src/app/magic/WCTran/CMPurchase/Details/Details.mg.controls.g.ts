import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Details = "Details",
        Label1048579 = "Label1048579",
        Label1048580 = "Label1048580",
        Label1048581 = "Label1048581",
        Label1048582 = "Label1048582",
        Label1048583 = "Label1048583",
        Label1048584 = "Label1048584",
        Label1048585 = "Label1048585",
        Label1048586 = "Label1048586",
        Label1048587 = "Label1048587",
        Label1048588 = "Label1048588",
        Label1048589 = "Label1048589",
        Label1048577 = "Label1048577",
        Label1048578 = "Label1048578",
        Label1048590 = "Label1048590",
        ItemName1 = "ItemName1",
        btnItem = "btnItem",
        vItemQty = "vItemQty",
        Unit = "Unit",
        vRate = "vRate",
        vAmt = "vAmt",
        vDisc_Percentage = "vDisc_Percentage",
        vDisc_Amt = "vDisc_Amt",
        vTotal = "vTotal",
        vCGST = "vCGST",
        vSGST = "vSGST",
        vIGST_0001 = "vIGST_0001",
        vbtnAdd = "vbtnAdd",
        BtnClear = "BtnClear",
        NewItem = "NewItem",
        Label1048645 = "Label1048645",
        vAvailableQty = "vAvailableQty",
        Label1048600 = "Label1048600",
        vMcode = "vMcode",
        Label1048603 = "Label1048603",
        vUserDesc = "vUserDesc",
        Table1048610 = "Table1048610",
        Column1048611 = "Column1048611",
        Edit1048628 = "Edit1048628",
        Column1048612 = "Column1048612",
        Item_Quantity = "Item_Quantity",
        Column1048613 = "Column1048613",
        Unit1 = "Unit1",
        Column1048614 = "Column1048614",
        Item_Price = "Item_Price",
        Column1048615 = "Column1048615",
        Basic = "Basic",
        Column1048616 = "Column1048616",
        Discount_Percentage = "Discount_Percentage",
        Column1048617 = "Column1048617",
        Discount_Amount = "Discount_Amount",
        Column1048618 = "Column1048618",
        Total = "Total",
        Column1048619 = "Column1048619",
        CGST_Id = "CGST_Id",
        Column1048620 = "Column1048620",
        CGSTAmt = "CGSTAmt",
        Column1048621 = "Column1048621",
        SGST_Id = "SGST_Id",
        Column1048622 = "Column1048622",
        SGSTAmt = "SGSTAmt",
        Column1048623 = "Column1048623",
        IGST_Id = "IGST_Id",
        Column1048624 = "Column1048624",
        IGSTAmt = "IGSTAmt",
        Column1048625 = "Column1048625",
        TotalTax = "TotalTax",
        Column1048626 = "Column1048626",
        TotalWttax = "TotalWttax",
        Column1048627 = "Column1048627",
        BtnDelete2 = "BtnDelete2",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column1048611',
        'Column1048612',
        'Column1048613',
        'Column1048614',
        'Column1048615',
        'Column1048616',
        'Column1048617',
        'Column1048618',
        'Column1048619',
        'Column1048620',
        'Column1048621',
        'Column1048622',
        'Column1048623',
        'Column1048624',
        'Column1048625',
        'Column1048626',
        'Column1048627',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get ItemName1(): FormControl {
        return this.fg.controls[MgControlName.ItemName1] as FormControl;
    }

    get vItemQty(): FormControl {
        return this.fg.controls[MgControlName.vItemQty] as FormControl;
    }

    get Unit(): FormControl {
        return this.fg.controls[MgControlName.Unit] as FormControl;
    }

    get vRate(): FormControl {
        return this.fg.controls[MgControlName.vRate] as FormControl;
    }

    get vAmt(): FormControl {
        return this.fg.controls[MgControlName.vAmt] as FormControl;
    }

    get vDisc_Percentage(): FormControl {
        return this.fg.controls[MgControlName.vDisc_Percentage] as FormControl;
    }

    get vDisc_Amt(): FormControl {
        return this.fg.controls[MgControlName.vDisc_Amt] as FormControl;
    }

    get vTotal(): FormControl {
        return this.fg.controls[MgControlName.vTotal] as FormControl;
    }

    get vCGST(): FormControl {
        return this.fg.controls[MgControlName.vCGST] as FormControl;
    }

    get vSGST(): FormControl {
        return this.fg.controls[MgControlName.vSGST] as FormControl;
    }

    get vIGST_0001(): FormControl {
        return this.fg.controls[MgControlName.vIGST_0001] as FormControl;
    }

    get vAvailableQty(): FormControl {
        return this.fg.controls[MgControlName.vAvailableQty] as FormControl;
    }

    get vMcode(): FormControl {
        return this.fg.controls[MgControlName.vMcode] as FormControl;
    }

    get vUserDesc(): FormControl {
        return this.fg.controls[MgControlName.vUserDesc] as FormControl;
    }

    get Edit1048628(): FormControl {
        return this.getTableChildFormControl(MgControlName.Edit1048628);
    }

    get Item_Quantity(): FormControl {
        return this.getTableChildFormControl(MgControlName.Item_Quantity);
    }

    get Unit1(): FormControl {
        return this.getTableChildFormControl(MgControlName.Unit1);
    }

    get Item_Price(): FormControl {
        return this.getTableChildFormControl(MgControlName.Item_Price);
    }

    get Basic(): FormControl {
        return this.getTableChildFormControl(MgControlName.Basic);
    }

    get Discount_Percentage(): FormControl {
        return this.getTableChildFormControl(MgControlName.Discount_Percentage);
    }

    get Discount_Amount(): FormControl {
        return this.getTableChildFormControl(MgControlName.Discount_Amount);
    }

    get Total(): FormControl {
        return this.getTableChildFormControl(MgControlName.Total);
    }

    get CGST_Id(): FormControl {
        return this.getTableChildFormControl(MgControlName.CGST_Id);
    }

    get CGSTAmt(): FormControl {
        return this.getTableChildFormControl(MgControlName.CGSTAmt);
    }

    get SGST_Id(): FormControl {
        return this.getTableChildFormControl(MgControlName.SGST_Id);
    }

    get SGSTAmt(): FormControl {
        return this.getTableChildFormControl(MgControlName.SGSTAmt);
    }

    get IGST_Id(): FormControl {
        return this.getTableChildFormControl(MgControlName.IGST_Id);
    }

    get IGSTAmt(): FormControl {
        return this.getTableChildFormControl(MgControlName.IGSTAmt);
    }

    get TotalTax(): FormControl {
        return this.getTableChildFormControl(MgControlName.TotalTax);
    }

    get TotalWttax(): FormControl {
        return this.getTableChildFormControl(MgControlName.TotalWttax);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}