import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMPurchaseTest = "CMPurchaseTest",
        Label1048580 = "Label1048580",
        PurchaseCode = "PurchaseCode",
        Label1048593 = "Label1048593",
        LogDate = "LogDate",
        Label1048584 = "Label1048584",
        RefNo = "RefNo",
        Label1048586 = "Label1048586",
        SupplierName = "SupplierName",
        btnCustSupp = "btnCustSupp",
        Label1048589 = "Label1048589",
        ChallanNoDate = "ChallanNoDate",
        Label1048591 = "Label1048591",
        InvoiceNoDate = "InvoiceNoDate",
        Label1048596 = "Label1048596",
        Basic = "Basic",
        Label1048598 = "Label1048598",
        Tax = "Tax",
        Label1048600 = "Label1048600",
        Total = "Total",
        Details = "Details",
        Save = "Save",
        Cancel = "Cancel",
        Active = "Active",
}
export enum MgCustomProperties {
    CMPurchaseTest_FormName = 'CMPurchaseTest~FormName',
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get PurchaseCode(): FormControl {
        return this.fg.controls[MgControlName.PurchaseCode] as FormControl;
    }

    get LogDate(): FormControl {
        return this.fg.controls[MgControlName.LogDate] as FormControl;
    }

    get RefNo(): FormControl {
        return this.fg.controls[MgControlName.RefNo] as FormControl;
    }

    get SupplierName(): FormControl {
        return this.fg.controls[MgControlName.SupplierName] as FormControl;
    }

    get ChallanNoDate(): FormControl {
        return this.fg.controls[MgControlName.ChallanNoDate] as FormControl;
    }

    get InvoiceNoDate(): FormControl {
        return this.fg.controls[MgControlName.InvoiceNoDate] as FormControl;
    }

    get Basic(): FormControl {
        return this.fg.controls[MgControlName.Basic] as FormControl;
    }

    get Tax(): FormControl {
        return this.fg.controls[MgControlName.Tax] as FormControl;
    }

    get Total(): FormControl {
        return this.fg.controls[MgControlName.Total] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}