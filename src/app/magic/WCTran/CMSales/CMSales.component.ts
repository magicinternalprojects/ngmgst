import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CMSales.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CMSales',
    providers: [...magicProviders],
    styleUrls: ['./CMSales.component.css'],
    templateUrl: './CMSales.component.html'
})
export class CMSales extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CMSales";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "80%";
    private static readonly height: string = "700px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CMSales.x;
    }
    Y() {
        return CMSales.y;
    }
    Width(): string {
        return CMSales.width;
    }
    Height(): string {
        return CMSales.height;
    }
    IsCenteredToWindow() {
        return CMSales.isCenteredToWindow;
    }
    FormName() {
        return CMSales.formName;
    }
    ShowTitleBar() {
        return CMSales.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CMSales.shouldCloseOnBackgroundClick;
    }
}