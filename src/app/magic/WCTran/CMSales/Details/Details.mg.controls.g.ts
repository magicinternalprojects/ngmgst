import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Details = "Details",
        Label1 = "Label1",
        Label16 = "Label16",
        Label17 = "Label17",
        Label5 = "Label5",
        Label26 = "Label26",
        ItemName = "ItemName",
        vAvailableQty = "vAvailableQty",
        Label6 = "Label6",
        vItemQty = "vItemQty",
        Label28 = "Label28",
        Label7 = "Label7",
        vUnit = "vUnit",
        vMcode = "vMcode",
        Label8 = "Label8",
        vRate = "vRate",
        Label9 = "Label9",
        vAmt = "vAmt",
        Label10 = "Label10",
        vDisc_Percentage = "vDisc_Percentage",
        Label30 = "Label30",
        vDisc_Amt = "vDisc_Amt",
        Label11 = "Label11",
        Label12 = "Label12",
        vTotal = "vTotal",
        vCustomerDescription = "vCustomerDescription",
        Label13 = "Label13",
        vCGST = "vCGST",
        Label14 = "Label14",
        vSGST = "vSGST",
        Active = "Active",
        Label15 = "Label15",
        vIGST = "vIGST",
        Label4 = "Label4",
        vCESSId = "vCESSId",
        Label2 = "Label2",
        vbtnAdd = "vbtnAdd",
        Label3 = "Label3",
        BtnClear = "BtnClear",
        Table38 = "Table38",
        Column39 = "Column39",
        Edit58 = "Edit58",
        Column40 = "Column40",
        Item_Quantity = "Item_Quantity",
        Column41 = "Column41",
        Unit = "Unit",
        Column42 = "Column42",
        Item_Price = "Item_Price",
        Column43 = "Column43",
        Basic = "Basic",
        Column44 = "Column44",
        Discount_Percentage = "Discount_Percentage",
        Column45 = "Column45",
        Discount_Amount = "Discount_Amount",
        Column46 = "Column46",
        Total = "Total",
        Column47 = "Column47",
        CGST_Id = "CGST_Id",
        Column48 = "Column48",
        CGSTAmt = "CGSTAmt",
        Column49 = "Column49",
        SGST_Id = "SGST_Id",
        Column50 = "Column50",
        SGSTAmt = "SGSTAmt",
        Column51 = "Column51",
        IGST_Id = "IGST_Id",
        Column52 = "Column52",
        IGSTAmt = "IGSTAmt",
        Column53 = "Column53",
        CESSId = "CESSId",
        Column54 = "Column54",
        CESSAmt = "CESSAmt",
        Column55 = "Column55",
        TotalTax = "TotalTax",
        Column56 = "Column56",
        TotalWttax = "TotalWttax",
        Column57 = "Column57",
        BtnDelete2 = "BtnDelete2",
        btn123 = "btn123",
        btnItem = "btnItem",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column39',
        'Column40',
        'Column41',
        'Column42',
        'Column43',
        'Column44',
        'Column45',
        'Column46',
        'Column47',
        'Column48',
        'Column49',
        'Column50',
        'Column51',
        'Column52',
        'Column53',
        'Column54',
        'Column55',
        'Column56',
        'Column57',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get ItemName(): FormControl {
        return this.fg.controls[MgControlName.ItemName] as FormControl;
    }

    get vAvailableQty(): FormControl {
        return this.fg.controls[MgControlName.vAvailableQty] as FormControl;
    }

    get vItemQty(): FormControl {
        return this.fg.controls[MgControlName.vItemQty] as FormControl;
    }

    get vUnit(): FormControl {
        return this.fg.controls[MgControlName.vUnit] as FormControl;
    }

    get vMcode(): FormControl {
        return this.fg.controls[MgControlName.vMcode] as FormControl;
    }

    get vRate(): FormControl {
        return this.fg.controls[MgControlName.vRate] as FormControl;
    }

    get vAmt(): FormControl {
        return this.fg.controls[MgControlName.vAmt] as FormControl;
    }

    get vDisc_Percentage(): FormControl {
        return this.fg.controls[MgControlName.vDisc_Percentage] as FormControl;
    }

    get vDisc_Amt(): FormControl {
        return this.fg.controls[MgControlName.vDisc_Amt] as FormControl;
    }

    get vTotal(): FormControl {
        return this.fg.controls[MgControlName.vTotal] as FormControl;
    }

    get vCustomerDescription(): FormControl {
        return this.fg.controls[MgControlName.vCustomerDescription] as FormControl;
    }

    get vCGST(): FormControl {
        return this.fg.controls[MgControlName.vCGST] as FormControl;
    }

    get vSGST(): FormControl {
        return this.fg.controls[MgControlName.vSGST] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    get vIGST(): FormControl {
        return this.fg.controls[MgControlName.vIGST] as FormControl;
    }

    get vCESSId(): FormControl {
        return this.fg.controls[MgControlName.vCESSId] as FormControl;
    }

    get Edit58(): FormControl {
        return this.getTableChildFormControl(MgControlName.Edit58);
    }

    get Item_Quantity(): FormControl {
        return this.getTableChildFormControl(MgControlName.Item_Quantity);
    }

    get Unit(): FormControl {
        return this.getTableChildFormControl(MgControlName.Unit);
    }

    get Item_Price(): FormControl {
        return this.getTableChildFormControl(MgControlName.Item_Price);
    }

    get Basic(): FormControl {
        return this.getTableChildFormControl(MgControlName.Basic);
    }

    get Discount_Percentage(): FormControl {
        return this.getTableChildFormControl(MgControlName.Discount_Percentage);
    }

    get Discount_Amount(): FormControl {
        return this.getTableChildFormControl(MgControlName.Discount_Amount);
    }

    get Total(): FormControl {
        return this.getTableChildFormControl(MgControlName.Total);
    }

    get CGST_Id(): FormControl {
        return this.getTableChildFormControl(MgControlName.CGST_Id);
    }

    get CGSTAmt(): FormControl {
        return this.getTableChildFormControl(MgControlName.CGSTAmt);
    }

    get SGST_Id(): FormControl {
        return this.getTableChildFormControl(MgControlName.SGST_Id);
    }

    get SGSTAmt(): FormControl {
        return this.getTableChildFormControl(MgControlName.SGSTAmt);
    }

    get IGST_Id(): FormControl {
        return this.getTableChildFormControl(MgControlName.IGST_Id);
    }

    get IGSTAmt(): FormControl {
        return this.getTableChildFormControl(MgControlName.IGSTAmt);
    }

    get CESSId(): FormControl {
        return this.getTableChildFormControl(MgControlName.CESSId);
    }

    get CESSAmt(): FormControl {
        return this.getTableChildFormControl(MgControlName.CESSAmt);
    }

    get TotalTax(): FormControl {
        return this.getTableChildFormControl(MgControlName.TotalTax);
    }

    get TotalWttax(): FormControl {
        return this.getTableChildFormControl(MgControlName.TotalWttax);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}