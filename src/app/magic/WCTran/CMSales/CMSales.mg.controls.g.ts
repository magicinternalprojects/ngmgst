import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    CMSales = "CMSales",
        Label4 = "Label4",
        SalesCode = "SalesCode",
        Label6 = "Label6",
        FYYear = "FYYear",
        Label8 = "Label8",
        RefNo = "RefNo",
        Label10 = "Label10",
        SupplierName = "SupplierName",
        Label13 = "Label13",
        ShippTo = "ShippTo",
        Label16 = "Label16",
        Edit36 = "Edit36",
        Active = "Active",
        Label18 = "Label18",
        Basic = "Basic",
        Label20 = "Label20",
        Tax = "Tax",
        Label22 = "Label22",
        Total = "Total",
        Label24 = "Label24",
        Remark = "Remark",
        Label26 = "Label26",
        GrossDisc = "GrossDisc",
        Label28 = "Label28",
        TotalWtDisc = "TotalWtDisc",
        Details = "Details",
        Label30 = "Label30",
        Label31 = "Label31",
        Save = "Save",
        Cancel = "Cancel",
        btnShipTo = "btnShipTo",
        btnCustomer = "btnCustomer",
}
export enum MgCustomProperties {
    CMSales_FormName = 'CMSales~FormName',
}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get SalesCode(): FormControl {
        return this.fg.controls[MgControlName.SalesCode] as FormControl;
    }

    get FYYear(): FormControl {
        return this.fg.controls[MgControlName.FYYear] as FormControl;
    }

    get RefNo(): FormControl {
        return this.fg.controls[MgControlName.RefNo] as FormControl;
    }

    get SupplierName(): FormControl {
        return this.fg.controls[MgControlName.SupplierName] as FormControl;
    }

    get ShippTo(): FormControl {
        return this.fg.controls[MgControlName.ShippTo] as FormControl;
    }

    get Edit36(): FormControl {
        return this.fg.controls[MgControlName.Edit36] as FormControl;
    }

    get Active(): FormControl {
        return this.fg.controls[MgControlName.Active] as FormControl;
    }

    get Basic(): FormControl {
        return this.fg.controls[MgControlName.Basic] as FormControl;
    }

    get Tax(): FormControl {
        return this.fg.controls[MgControlName.Tax] as FormControl;
    }

    get Total(): FormControl {
        return this.fg.controls[MgControlName.Total] as FormControl;
    }

    get Remark(): FormControl {
        return this.fg.controls[MgControlName.Remark] as FormControl;
    }

    get GrossDisc(): FormControl {
        return this.fg.controls[MgControlName.GrossDisc] as FormControl;
    }

    get TotalWtDisc(): FormControl {
        return this.fg.controls[MgControlName.TotalWtDisc] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}