import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    PurchaseList = "PurchaseList",
        vSearchText = "vSearchText",
        vFromDate = "vFromDate",
        vToDate = "vToDate",
        Search = "Search",
        Reset = "Reset",
        Detail = "Detail",
}
export enum MgCustomProperties {}
export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get vSearchText(): FormControl {
        return this.fg.controls[MgControlName.vSearchText] as FormControl;
    }

    get vFromDate(): FormControl {
        return this.fg.controls[MgControlName.vFromDate] as FormControl;
    }

    get vToDate(): FormControl {
        return this.fg.controls[MgControlName.vToDate] as FormControl;
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}