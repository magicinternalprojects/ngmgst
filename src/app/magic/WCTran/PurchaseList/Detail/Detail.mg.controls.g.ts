import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        New = "New",
        Table1 = "Table1",
        Column5 = "Column5",
        PurchaseCode = "PurchaseCode",
        Column6 = "Column6",
        CustName = "CustName",
        Column7 = "Column7",
        RefNo = "RefNo",
        Column8 = "Column8",
        ChallanNoDate = "ChallanNoDate",
        Column9 = "Column9",
        InvoiceNoDate = "InvoiceNoDate",
        Column10 = "Column10",
        Basic = "Basic",
        Column11 = "Column11",
        Tax = "Tax",
        Column12 = "Column12",
        Total = "Total",
        Column13 = "Column13",
        Active = "Active",
        Column14 = "Column14",
        FullName = "FullName",
        Column1048605 = "Column1048605",
        Edit = "Edit",
        View_Detail = "View_Detail",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column5',
        'Column6',
        'Column8',
        'Column9',
        'Column10',
        'Column11',
        'Column12',
        'Column1048605',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get PurchaseCode(): FormControl {
        return this.getTableChildFormControl(MgControlName.PurchaseCode);
    }

    get CustName(): FormControl {
        return this.getTableChildFormControl(MgControlName.CustName);
    }

    get RefNo(): FormControl {
        return this.getTableChildFormControl(MgControlName.RefNo);
    }

    get ChallanNoDate(): FormControl {
        return this.getTableChildFormControl(MgControlName.ChallanNoDate);
    }

    get InvoiceNoDate(): FormControl {
        return this.getTableChildFormControl(MgControlName.InvoiceNoDate);
    }

    get Basic(): FormControl {
        return this.getTableChildFormControl(MgControlName.Basic);
    }

    get Tax(): FormControl {
        return this.getTableChildFormControl(MgControlName.Tax);
    }

    get Total(): FormControl {
        return this.getTableChildFormControl(MgControlName.Total);
    }

    get Active(): FormControl {
        return this.getTableChildFormControl(MgControlName.Active);
    }

    get FullName(): FormControl {
        return this.getTableChildFormControl(MgControlName.FullName);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}