import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./HSNNoSelection.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-HSNNoSelection',
    providers: [...magicProviders],
    templateUrl: './HSNNoSelection.component.html'
})
export class HSNNoSelection extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "HSN/SAC No Selection";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "600px";
    private static readonly height: string = "585px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return HSNNoSelection.x;
    }
    Y() {
        return HSNNoSelection.y;
    }
    Width(): string {
        return HSNNoSelection.width;
    }
    Height(): string {
        return HSNNoSelection.height;
    }
    IsCenteredToWindow() {
        return HSNNoSelection.isCenteredToWindow;
    }
    FormName() {
        return HSNNoSelection.formName;
    }
    ShowTitleBar() {
        return HSNNoSelection.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return HSNNoSelection.shouldCloseOnBackgroundClick;
    }
}