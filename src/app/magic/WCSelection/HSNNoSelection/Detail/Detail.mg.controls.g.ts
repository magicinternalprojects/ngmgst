import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        Table1048578 = "Table1048578",
        Column1048579 = "Column1048579",
        HSNNo = "HSNNo",
        Column1048580 = "Column1048580",
        TaxCGST = "TaxCGST",
        Column1048581 = "Column1048581",
        TaxSGST = "TaxSGST",
        Column1048582 = "Column1048582",
        TaxIGST = "TaxIGST",
        Label1048577 = "Label1048577",
        Select = "Select",
        New = "New",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column1048579',
        'Column1048580',
        'Column1048581',
        'Column1048582',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get HSNNo(): FormControl {
        return this.getTableChildFormControl(MgControlName.HSNNo);
    }

    get TaxCGST(): FormControl {
        return this.getTableChildFormControl(MgControlName.TaxCGST);
    }

    get TaxSGST(): FormControl {
        return this.getTableChildFormControl(MgControlName.TaxSGST);
    }

    get TaxIGST(): FormControl {
        return this.getTableChildFormControl(MgControlName.TaxIGST);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}