import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CustomerSupplierSelection.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CustomerSupplierSelection',
    providers: [...magicProviders],
    templateUrl: './CustomerSupplierSelection.component.html'
})
export class CustomerSupplierSelection extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "Customer/Supplier Selection";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "600px";
    private static readonly height: string = "585px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CustomerSupplierSelection.x;
    }
    Y() {
        return CustomerSupplierSelection.y;
    }
    Width(): string {
        return CustomerSupplierSelection.width;
    }
    Height(): string {
        return CustomerSupplierSelection.height;
    }
    IsCenteredToWindow() {
        return CustomerSupplierSelection.isCenteredToWindow;
    }
    FormName() {
        return CustomerSupplierSelection.formName;
    }
    ShowTitleBar() {
        return CustomerSupplierSelection.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CustomerSupplierSelection.shouldCloseOnBackgroundClick;
    }
}