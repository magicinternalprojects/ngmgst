import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./BranchSelection.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-BranchSelection',
    providers: [...magicProviders],
    templateUrl: './BranchSelection.component.html'
})
export class BranchSelection extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "BranchSelection";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "600px";
    private static readonly height: string = "585px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return BranchSelection.x;
    }
    Y() {
        return BranchSelection.y;
    }
    Width(): string {
        return BranchSelection.width;
    }
    Height(): string {
        return BranchSelection.height;
    }
    IsCenteredToWindow() {
        return BranchSelection.isCenteredToWindow;
    }
    FormName() {
        return BranchSelection.formName;
    }
    ShowTitleBar() {
        return BranchSelection.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return BranchSelection.shouldCloseOnBackgroundClick;
    }
}