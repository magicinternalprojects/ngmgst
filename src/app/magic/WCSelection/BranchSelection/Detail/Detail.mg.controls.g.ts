import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        Table2 = "Table2",
        Column3 = "Column3",
        TableRow = "TableRow",
        Column4 = "Column4",
        BranchLocation_0001 = "BranchLocation_0001",
        Select = "Select",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column3',
        'Column4',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get TableRow(): FormControl {
        return this.getTableChildFormControl(MgControlName.TableRow);
    }

    get BranchLocation_0001(): FormControl {
        return this.getTableChildFormControl(MgControlName.BranchLocation_0001);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}