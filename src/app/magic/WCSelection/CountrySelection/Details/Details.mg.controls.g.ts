import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Details = "Details",
        Table1048578 = "Table1048578",
        Column1048579 = "Column1048579",
        Name = "Name",
        Column1048580 = "Column1048580",
        ShortName = "ShortName",
        Select = "Select",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column1048579',
        'Column1048580',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get Name(): FormControl {
        return this.getTableChildFormControl(MgControlName.Name);
    }

    get ShortName(): FormControl {
        return this.getTableChildFormControl(MgControlName.ShortName);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}