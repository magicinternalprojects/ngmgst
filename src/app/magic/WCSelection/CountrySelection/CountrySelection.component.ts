import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./CountrySelection.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-CountrySelection',
    providers: [...magicProviders],
    templateUrl: './CountrySelection.component.html'
})
export class CountrySelection extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "CountrySelection";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "600px";
    private static readonly height: string = "585px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return CountrySelection.x;
    }
    Y() {
        return CountrySelection.y;
    }
    Width(): string {
        return CountrySelection.width;
    }
    Height(): string {
        return CountrySelection.height;
    }
    IsCenteredToWindow() {
        return CountrySelection.isCenteredToWindow;
    }
    FormName() {
        return CountrySelection.formName;
    }
    ShowTitleBar() {
        return CountrySelection.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return CountrySelection.shouldCloseOnBackgroundClick;
    }
}