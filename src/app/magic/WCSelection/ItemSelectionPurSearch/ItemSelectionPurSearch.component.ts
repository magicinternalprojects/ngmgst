import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./ItemSelectionPurSearch.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-ItemSelectionPurSearch',
    providers: [...magicProviders],
    templateUrl: './ItemSelectionPurSearch.component.html'
})
export class ItemSelectionPurSearch extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "ItemSelectionPurSearch";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "600px";
    private static readonly height: string = "585px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return ItemSelectionPurSearch.x;
    }
    Y() {
        return ItemSelectionPurSearch.y;
    }
    Width(): string {
        return ItemSelectionPurSearch.width;
    }
    Height(): string {
        return ItemSelectionPurSearch.height;
    }
    IsCenteredToWindow() {
        return ItemSelectionPurSearch.isCenteredToWindow;
    }
    FormName() {
        return ItemSelectionPurSearch.formName;
    }
    ShowTitleBar() {
        return ItemSelectionPurSearch.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return ItemSelectionPurSearch.shouldCloseOnBackgroundClick;
    }
}