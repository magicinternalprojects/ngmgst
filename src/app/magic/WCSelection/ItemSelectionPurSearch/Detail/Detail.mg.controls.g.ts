import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        Table1048578 = "Table1048578",
        Column1048579 = "Column1048579",
        Description = "Description",
        Column1048580 = "Column1048580",
        HSNNo = "HSNNo",
        Column1048581 = "Column1048581",
        Stock = "Stock",
        Column1048582 = "Column1048582",
        MyRecords = "MyRecords",
        Select = "Select",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column1048579',
        'Column1048580',
        'Column1048581',
        'Column1048582',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get Description(): FormControl {
        return this.getTableChildFormControl(MgControlName.Description);
    }

    get HSNNo(): FormControl {
        return this.getTableChildFormControl(MgControlName.HSNNo);
    }

    get Stock(): FormControl {
        return this.getTableChildFormControl(MgControlName.Stock);
    }

    get MyRecords(): FormControl {
        return this.getTableChildFormControl(MgControlName.MyRecords);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}