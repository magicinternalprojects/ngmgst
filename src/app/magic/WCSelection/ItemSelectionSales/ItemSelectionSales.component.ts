import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./ItemSelectionSales.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-ItemSelectionSales',
    providers: [...magicProviders],
    templateUrl: './ItemSelectionSales.component.html'
})
export class ItemSelectionSales extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "ItemSelectionSales";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "600px";
    private static readonly height: string = "585px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return ItemSelectionSales.x;
    }
    Y() {
        return ItemSelectionSales.y;
    }
    Width(): string {
        return ItemSelectionSales.width;
    }
    Height(): string {
        return ItemSelectionSales.height;
    }
    IsCenteredToWindow() {
        return ItemSelectionSales.isCenteredToWindow;
    }
    FormName() {
        return ItemSelectionSales.formName;
    }
    ShowTitleBar() {
        return ItemSelectionSales.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return ItemSelectionSales.shouldCloseOnBackgroundClick;
    }
}