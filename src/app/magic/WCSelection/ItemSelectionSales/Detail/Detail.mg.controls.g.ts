import {
    FormControl,
    FormGroup
} from "@angular/forms";
import {
    MagicServices
} from "@magic-xpa/angular";
export enum MgControlName {
    Detail = "Detail",
        Table2 = "Table2",
        Column3 = "Column3",
        Description_0001 = "Description_0001",
        Column4 = "Column4",
        TableRow = "TableRow",
        Column5 = "Column5",
        vIn_Qty = "vIn_Qty",
        Column6 = "Column6",
        MyRecords = "MyRecords",
        BtnSelect = "BtnSelect",
        Cancel = "Cancel",
}
export enum MgCustomProperties {}
export var
    MgDisplayedColumns = [
        'Column3',
        'Column4',
        'Column5',
        'Column6',
    ];

export class MgFormControlsAccessor {
    constructor(private fg: FormGroup, private magicServices: MagicServices) {}

    get Description_0001(): FormControl {
        return this.getTableChildFormControl(MgControlName.Description_0001);
    }

    get TableRow(): FormControl {
        return this.getTableChildFormControl(MgControlName.TableRow);
    }

    get vIn_Qty(): FormControl {
        return this.getTableChildFormControl(MgControlName.vIn_Qty);
    }

    get MyRecords(): FormControl {
        return this.getTableChildFormControl(MgControlName.MyRecords);
    }

    getTableChildFormControl(name: MgControlName): FormControl {
        return this.magicServices.mgAccessorService.getFormGroupByRow(this.magicServices.tableService.getSelectedRow()).controls[name] as FormControl;
    }
}