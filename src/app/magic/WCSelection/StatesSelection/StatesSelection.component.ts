import { Component } from '@angular/core';

import { FormGroup } from "@angular/forms";
import { MgFormControlsAccessor, MgControlName, MgCustomProperties } from "./StatesSelection.mg.controls.g";


import { TaskBaseMagicComponent, magicProviders } from "@magic-xpa/angular";


import { MagicModalInterface } from "@magic-xpa/angular";

@Component({
    selector: 'mga-StatesSelection',
    providers: [...magicProviders],
    templateUrl: './StatesSelection.component.html'
})
export class StatesSelection extends TaskBaseMagicComponent implements MagicModalInterface {

    mgc = MgControlName;
    mgcp = MgCustomProperties;
    mgfc: MgFormControlsAccessor;
    createFormControlsAccessor(formGroup: FormGroup) {
        this.mgfc = new MgFormControlsAccessor(formGroup, this.magicServices);
    }
    private static readonly formName: string = "StatesSelection";
    private static readonly showTitleBar: boolean = false;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: string = "600px";
    private static readonly height: string = "585px";
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = false;
    X() {
        return StatesSelection.x;
    }
    Y() {
        return StatesSelection.y;
    }
    Width(): string {
        return StatesSelection.width;
    }
    Height(): string {
        return StatesSelection.height;
    }
    IsCenteredToWindow() {
        return StatesSelection.isCenteredToWindow;
    }
    FormName() {
        return StatesSelection.formName;
    }
    ShowTitleBar() {
        return StatesSelection.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return StatesSelection.shouldCloseOnBackgroundClick;
    }
}