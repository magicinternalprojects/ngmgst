import { Routes, RouterModule } from '@angular/router';

import { RouterContainerMagicComponent } from "@magic-xpa/angular";
import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';
export const routes: Routes = [

    {
        path: 'Login',
        component: RouterContainerMagicComponent,

    },
    {
        path: 'HomePage',
        component: RouterContainerMagicComponent,


        children: [{
                path: 'CompanyMast',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'PurchaseList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'CustSuppList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'ItemMasterList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'TaxMasterList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'CountryList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'StateList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'CityList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'ViewUnitMaster',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'BranchList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'FinancialYearList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'HSNMasterList',
                component: RouterContainerMagicComponent,

            },
            {
                path: 'SalesList',
                component: RouterContainerMagicComponent,

            },
        ]
    },
    {
        path: 'UserCompanyList',
        component: RouterContainerMagicComponent,

    },

];

@NgModule({
    imports: [CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class MagicRoutingModule {}